#include <webkit/webkit.h>

#include <assert.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/fail.h>
#include <caml/custom.h>
#include <caml/callback.h>
#include <caml/alloc.h>
#include <wrappers.h>
#include <ml_glib.h>
#include <ml_gtk.h>
#include <ml_gobject.h>
#include <gtk_tags.h>

#ifndef COWBOY_GLIB_CONVERSIONS_H
#define COWBOY_GLIB_CONVERSIONS_H

#define Val_double caml_copy_double
#define Val_float caml_copy_double
#define Uint_val Int_val
#define Val_uint Val_int
#define Gssize_val Int32_val
#define Uint64_t_val Int64_val
#define Val_uint64_t caml_copy_int64

#define GtkMovementStep_val Movement_step_val
#define Val_gtk_widget Val_GtkWidget
#define GdkEventButton_val(val) check_cast(GTK_BUTTON,val)
#define Val_gtk_policy_type Val_policy_type
#define Val_gstring(s) (s->str)

#define Val_gtype Val_GType

#endif /* COWBOY_GLIB_CONVERSIONS_H */
#include "WebBackForwardList/ml_gtkWebBackForwardList_tags_h.h"

#define WebkitWebBackForwardList_val(val) check_cast(WEBKIT_WEB_BACK_FORWARD_LIST,val)
#define Val_webkit_web_back_forward_list(val) Val_GtkAny(val)
#include "Download/ml_gtkDownload_tags_h.h"

#define WebkitDownload_val(val) check_cast(WEBKIT_DOWNLOAD,val)
#define Val_webkit_download(val) Val_GtkAny(val)
#include "WebPolicyDecision/ml_gtkWebPolicyDecision_tags_h.h"

#define WebkitWebPolicyDecision_val(val) check_cast(WEBKIT_WEB_POLICY_DECISION,val)
#define Val_webkit_web_policy_decision(val) Val_GtkAny(val)
#include "Error/ml_gtkError_tags_h.h"

#define WebkitError_val(val) check_cast(WEBKIT_ERROR,val)
#define Val_webkit_error(val) Val_GtkAny(val)
#include "NetworkResponse/ml_gtkNetworkResponse_tags_h.h"

#define WebkitNetworkResponse_val(val) check_cast(WEBKIT_NETWORK_RESPONSE,val)
#define Val_webkit_network_response(val) Val_GtkAny(val)
#include "Domdefines/ml_gtkDomdefines_tags_h.h"

#define WebkitDomdefines_val(val) check_cast(WEBKIT_DOMDEFINES,val)
#define Val_webkit_domdefines(val) Val_GtkAny(val)
#include "Defines/ml_gtkDefines_tags_h.h"

#define WebkitDefines_val(val) check_cast(WEBKIT_DEFINES,val)
#define Val_webkit_defines(val) Val_GtkAny(val)
#include "Dom/ml_gtkDom_tags_h.h"

#define WebkitDom_val(val) check_cast(WEBKIT_DOM,val)
#define Val_webkit_dom(val) Val_GtkAny(val)
#include "Version/ml_gtkVersion_tags_h.h"

#define WebkitVersion_val(val) check_cast(WEBKIT_VERSION,val)
#define Val_webkit_version(val) Val_GtkAny(val)
#include "ViewportAttributes/ml_gtkViewportAttributes_tags_h.h"

#define WebkitViewportAttributes_val(val) check_cast(WEBKIT_VIEWPORT_ATTRIBUTES,val)
#define Val_webkit_viewport_attributes(val) Val_GtkAny(val)
#include "WebSettings/ml_gtkWebSettings_tags_h.h"

#define WebkitWebSettings_val(val) check_cast(WEBKIT_WEB_SETTINGS,val)
#define Val_webkit_web_settings(val) Val_GtkAny(val)
#include "WebInspector/ml_gtkWebInspector_tags_h.h"

#define WebkitWebInspector_val(val) check_cast(WEBKIT_WEB_INSPECTOR,val)
#define Val_webkit_web_inspector(val) Val_GtkAny(val)
#include "WebView/ml_gtkWebView_tags_h.h"

#define WebkitWebView_val(val) check_cast(WEBKIT_WEB_VIEW,val)
#define Val_webkit_web_view(val) Val_GtkAny(val)
#include "SpellChecker/ml_gtkSpellChecker_tags_h.h"

#define WebkitSpellChecker_val(val) check_cast(WEBKIT_SPELL_CHECKER,val)
#define Val_webkit_spell_checker(val) Val_GtkAny(val)
#include "FaviconDatabase/ml_gtkFaviconDatabase_tags_h.h"

#define WebkitFaviconDatabase_val(val) check_cast(WEBKIT_FAVICON_DATABASE,val)
#define Val_webkit_favicon_database(val) Val_GtkAny(val)
#include "WebResource/ml_gtkWebResource_tags_h.h"

#define WebkitWebResource_val(val) check_cast(WEBKIT_WEB_RESOURCE,val)
#define Val_webkit_web_resource(val) Val_GtkAny(val)
#include "NetworkRequest/ml_gtkNetworkRequest_tags_h.h"

#define WebkitNetworkRequest_val(val) check_cast(WEBKIT_NETWORK_REQUEST,val)
#define Val_webkit_network_request(val) Val_GtkAny(val)
#include "Applicationcache/ml_gtkApplicationcache_tags_h.h"

#define WebkitApplicationcache_val(val) check_cast(WEBKIT_APPLICATIONCACHE,val)
#define Val_webkit_applicationcache(val) Val_GtkAny(val)
#include "IconDatabase/ml_gtkIconDatabase_tags_h.h"

#define WebkitIconDatabase_val(val) check_cast(WEBKIT_ICON_DATABASE,val)
#define Val_webkit_icon_database(val) Val_GtkAny(val)
#include "WebHistoryItem/ml_gtkWebHistoryItem_tags_h.h"

#define WebkitWebHistoryItem_val(val) check_cast(WEBKIT_WEB_HISTORY_ITEM,val)
#define Val_webkit_web_history_item(val) Val_GtkAny(val)
#include "HitTestResult/ml_gtkHitTestResult_tags_h.h"

#define WebkitHitTestResult_val(val) check_cast(WEBKIT_HIT_TEST_RESULT,val)
#define Val_webkit_hit_test_result(val) Val_GtkAny(val)
#include "GeolocationPolicyDecision/ml_gtkGeolocationPolicyDecision_tags_h.h"

#define WebkitGeolocationPolicyDecision_val(val) check_cast(WEBKIT_GEOLOCATION_POLICY_DECISION,val)
#define Val_webkit_geolocation_policy_decision(val) Val_GtkAny(val)
#include "WebNavigationAction/ml_gtkWebNavigationAction_tags_h.h"

#define WebkitWebNavigationAction_val(val) check_cast(WEBKIT_WEB_NAVIGATION_ACTION,val)
#define Val_webkit_web_navigation_action(val) Val_GtkAny(val)
#include "SecurityOrigin/ml_gtkSecurityOrigin_tags_h.h"

#define WebkitSecurityOrigin_val(val) check_cast(WEBKIT_SECURITY_ORIGIN,val)
#define Val_webkit_security_origin(val) Val_GtkAny(val)
#include "WebDatabase/ml_gtkWebDatabase_tags_h.h"

#define WebkitWebDatabase_val(val) check_cast(WEBKIT_WEB_DATABASE,val)
#define Val_webkit_web_database(val) Val_GtkAny(val)
#include "WebWindowFeatures/ml_gtkWebWindowFeatures_tags_h.h"

#define WebkitWebWindowFeatures_val(val) check_cast(WEBKIT_WEB_WINDOW_FEATURES,val)
#define Val_webkit_web_window_features(val) Val_GtkAny(val)
#include "Globals/ml_gtkGlobals_tags_h.h"

#define WebkitGlobals_val(val) check_cast(WEBKIT_GLOBALS,val)
#define Val_webkit_globals(val) Val_GtkAny(val)
#include "WebPluginDatabase/ml_gtkWebPluginDatabase_tags_h.h"

#define WebkitWebPluginDatabase_val(val) check_cast(WEBKIT_WEB_PLUGIN_DATABASE,val)
#define Val_webkit_web_plugin_database(val) Val_GtkAny(val)
#include "WebDataSource/ml_gtkWebDataSource_tags_h.h"

#define WebkitWebDataSource_val(val) check_cast(WEBKIT_WEB_DATA_SOURCE,val)
#define Val_webkit_web_data_source(val) Val_GtkAny(val)
#include "WebPlugin/ml_gtkWebPlugin_tags_h.h"

#define WebkitWebPlugin_val(val) check_cast(WEBKIT_WEB_PLUGIN,val)
#define Val_webkit_web_plugin(val) Val_GtkAny(val)
#include "WebFrame/ml_gtkWebFrame_tags_h.h"

#define WebkitWebFrame_val(val) check_cast(WEBKIT_WEB_FRAME,val)
#define Val_webkit_web_frame(val) Val_GtkAny(val)
#include "EnumTypes/ml_gtkEnumTypes_tags_h.h"

#define WebkitEnumTypes_val(val) check_cast(WEBKIT_ENUM_TYPES,val)
#define Val_webkit_enum_types(val) Val_GtkAny(val)
