#include "ml_gtkWebkit.h"
#include "ml_gtkGlobals_tags_c.h"

#define WebkitCacheModel_val Webkit_cache_model_val

ML_0(webkit_get_web_plugin_database, Val_webkit_web_plugin_database)

ML_0(webkit_get_icon_database, Val_webkit_icon_database)

ML_0(webkit_get_favicon_database, Val_webkit_favicon_database)

ML_1(webkit_set_cache_model, WebkitCacheModel_val, Unit)

ML_0(webkit_get_cache_model, Val_webkit_cache_model)

ML_0(webkit_get_text_checker, Val_gobject)

ML_1(webkit_set_text_checker, Gobject_val, Unit)

