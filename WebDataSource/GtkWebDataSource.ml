open Gobject
open Data
open GtkWebkitTypes
module WebDataSource = struct
  external create : unit -> webdatasource Gtk.obj = "ml_webkit_web_data_source_new"
  external new_with_request : networkrequest Gtk.obj -> webdatasource Gtk.obj = "ml_webkit_web_data_source_new_with_request"
  external get_web_frame : webdatasource Gtk.obj -> webframe Gtk.obj = "ml_webkit_web_data_source_get_web_frame"
  external get_initial_request : webdatasource Gtk.obj -> networkrequest Gtk.obj = "ml_webkit_web_data_source_get_initial_request"
  external get_request : webdatasource Gtk.obj -> networkrequest Gtk.obj = "ml_webkit_web_data_source_get_request"
  external get_encoding : webdatasource Gtk.obj -> string = "ml_webkit_web_data_source_get_encoding"
  external is_loading : webdatasource Gtk.obj -> bool = "ml_webkit_web_data_source_is_loading"
  external get_data : webdatasource Gtk.obj -> string = "ml_webkit_web_data_source_get_data"
  external get_main_resource : webdatasource Gtk.obj -> webresource Gtk.obj = "ml_webkit_web_data_source_get_main_resource"
  external get_unreachable_uri : webdatasource Gtk.obj -> string = "ml_webkit_web_data_source_get_unreachable_uri"
end