open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebDataSource = struct
  let cast w : webdatasource obj = try_cast w "WebkitWebDataSource"
  let create pl : webdatasource obj =
    Gobject.unsafe_create "WebkitWebDataSource" pl
end

