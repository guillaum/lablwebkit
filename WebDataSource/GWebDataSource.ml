open GObj
open Gobject
open Gtk
open GtkWebDataSource
open OGtkWebDataSourceProps

class web_data_source_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  method get_web_frame = WebDataSource.get_web_frame obj_
  method get_initial_request = WebDataSource.get_initial_request obj_
  method get_request = WebDataSource.get_request obj_
  method get_encoding = WebDataSource.get_encoding obj_
  method is_loading = WebDataSource.is_loading obj_
  method get_data = WebDataSource.get_data obj_
  method get_main_resource = WebDataSource.get_main_resource obj_
  method get_unreachable_uri = WebDataSource.get_unreachable_uri obj_
end

class web_data_source obj_ = object(self)
  method as_webdatasource : GtkWebkitTypes.webdatasource obj = obj_
  inherit web_data_source_skel obj_
end

let web_data_source () =
  new web_data_source (WebDataSource.create ())

let web_data_source_with_request request =
  new web_data_source (WebDataSource.new_with_request request)

let wrap_webdatasource obj = new web_data_source (unsafe_cast obj)
let unwrap_webdatasource obj = unsafe_cast obj#as_webdatasource
let conv_webdatasource =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webdatasource c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webdatasource c))) }

