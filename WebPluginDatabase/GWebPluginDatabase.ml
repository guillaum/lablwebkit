open GObj
open Gobject
open Gtk
open GtkWebPluginDatabase
open OGtkWebPluginDatabaseProps

class web_plugin_database_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  method get_plugin_for_mimetype = WebPluginDatabase.get_plugin_for_mimetype obj_
  method refresh = WebPluginDatabase.refresh obj_
end

class web_plugin_database obj_ = object(self)
  method as_webplugindatabase : GtkWebkitTypes.webplugindatabase obj = obj_
  inherit web_plugin_database_skel obj_
end



let wrap_webplugindatabase obj = new web_plugin_database (unsafe_cast obj)
let unwrap_webplugindatabase obj = unsafe_cast obj#as_webplugindatabase
let conv_webplugindatabase =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webplugindatabase c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webplugindatabase c))) }

