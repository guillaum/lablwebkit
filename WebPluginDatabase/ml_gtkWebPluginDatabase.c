#include "ml_gtkWebkit.h"
#include "ml_gtkWebPluginDatabase_tags_c.h"



ML_0(webkit_web_plugin_database_get_type, Val_gtype)

ML_2(webkit_web_plugin_database_get_plugin_for_mimetype, WebkitWebPluginDatabase_val, Char_val, Val_webkit_web_plugin)

ML_1(webkit_web_plugin_database_refresh, WebkitWebPluginDatabase_val, Unit)

