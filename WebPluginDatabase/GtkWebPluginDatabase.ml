open Gobject
open Data
open GtkWebkitTypes
module WebPluginDatabase = struct
  
  external get_plugin_for_mimetype : webplugindatabase Gtk.obj -> char -> webplugin Gtk.obj = "ml_webkit_web_plugin_database_get_plugin_for_mimetype"
  external refresh : webplugindatabase Gtk.obj -> unit = "ml_webkit_web_plugin_database_refresh"
end