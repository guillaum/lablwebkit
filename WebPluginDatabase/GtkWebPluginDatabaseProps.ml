open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebPluginDatabase = struct
  let cast w : webplugindatabase obj = try_cast w "WebkitWebPluginDatabase"
  let create pl : webplugindatabase obj =
    Gobject.unsafe_create "WebkitWebPluginDatabase" pl
end

