open GObj
open Gobject
open Gtk
open GtkHitTestResult
open OGtkHitTestResultProps

class hit_test_result_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit hit_test_result_props
end

class hit_test_result obj_ = object(self)
  method as_hittestresult : GtkWebkitTypes.hittestresult obj = obj_
  inherit hit_test_result_skel obj_
end



let wrap_hittestresult obj = new hit_test_result (unsafe_cast obj)
let unwrap_hittestresult obj = unsafe_cast obj#as_hittestresult
let conv_hittestresult =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_hittestresult c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_hittestresult c))) }

