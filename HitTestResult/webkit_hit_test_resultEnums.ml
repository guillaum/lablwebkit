(** webkit_hit_test_result enums *)

open Gpointer

type webkit_hit_test_result_context =
  [ `EDITABLE | `SELECTION | `MEDIA | `IMAGE | `LINK | `DOCUMENT ]

(**/**)

external _get_tables : unit ->
    webkit_hit_test_result_context variant_table
  = "ml_webkit_hit_test_result_get_tables"


let webkit_hit_test_result_context = _get_tables ()

let webkit_hit_test_result_context_conv = Gobject.Data.enum webkit_hit_test_result_context
