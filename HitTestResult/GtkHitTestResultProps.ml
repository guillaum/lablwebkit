open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module HitTestResult = struct
  let cast w : hittestresult obj = try_cast w "WebkitHitTestResult"
  module P = struct
    let context : ([>`hittestresult],_) property =
      {name="context"; conv=webkit_hit_test_result_context_conv}
    let link_uri : ([>`hittestresult],_) property =
      {name="link-uri"; conv=string}
    let image_uri : ([>`hittestresult],_) property =
      {name="image-uri"; conv=string}
    let media_uri : ([>`hittestresult],_) property =
      {name="media-uri"; conv=string}
  end
  let create ?context ?link_uri ?image_uri ?media_uri pl : hittestresult obj =
    let pl = (
      may_cons P.context context (
      may_cons P.link_uri link_uri (
      may_cons P.image_uri image_uri (
      may_cons P.media_uri media_uri pl)))) in
    Gobject.unsafe_create "WebkitHitTestResult" pl
end

