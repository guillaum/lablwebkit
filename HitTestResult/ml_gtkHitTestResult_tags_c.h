/* webkit_hit_test_result_context : conversion table */
const lookup_info ml_table_webkit_hit_test_result_context[] = {
  { 0, 6 },
  { MLTAG_DOCUMENT, WEBKIT_HIT_TEST_RESULT_CONTEXT_DOCUMENT },
  { MLTAG_MEDIA, WEBKIT_HIT_TEST_RESULT_CONTEXT_MEDIA },
  { MLTAG_SELECTION, WEBKIT_HIT_TEST_RESULT_CONTEXT_SELECTION },
  { MLTAG_EDITABLE, WEBKIT_HIT_TEST_RESULT_CONTEXT_EDITABLE },
  { MLTAG_LINK, WEBKIT_HIT_TEST_RESULT_CONTEXT_LINK },
  { MLTAG_IMAGE, WEBKIT_HIT_TEST_RESULT_CONTEXT_IMAGE },
};

CAMLprim value ml_webkit_hit_test_result_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_hit_test_result_context,
  };
  return (value)ml_lookup_tables[0];}
