open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkHitTestResultProps

class virtual hit_test_result_props = object
  val virtual obj : _ obj
  method context = get HitTestResult.P.context obj
  method link_uri = get HitTestResult.P.link_uri obj
  method image_uri = get HitTestResult.P.image_uri obj
  method media_uri = get HitTestResult.P.media_uri obj
end

class virtual hit_test_result_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method context = self#notify HitTestResult.P.context
  method link_uri = self#notify HitTestResult.P.link_uri
  method image_uri = self#notify HitTestResult.P.image_uri
  method media_uri = self#notify HitTestResult.P.media_uri
end

