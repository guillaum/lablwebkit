/* webkit_hit_test_result_context : tags and macros */
#define MLTAG_EDITABLE	((value)(791385252*2+1))
#define MLTAG_SELECTION	((value)(158558252*2+1))
#define MLTAG_MEDIA	((value)(61488324*2+1))
#define MLTAG_IMAGE	((value)(995579707*2+1))
#define MLTAG_LINK	((value)(846454778*2+1))
#define MLTAG_DOCUMENT	((value)(-191332581*2+1))

extern const lookup_info ml_table_webkit_hit_test_result_context[];
#define Val_webkit_hit_test_result_context(data) ml_lookup_from_c (ml_table_webkit_hit_test_result_context, data)
#define Webkit_hit_test_result_context_val(key) ml_lookup_to_c (ml_table_webkit_hit_test_result_context, key)

