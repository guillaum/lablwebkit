open GObj
open Gobject
open Gtk
open GtkGeolocationPolicyDecision
open OGtkGeolocationPolicyDecisionProps

class geolocation_policy_decision_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  method allow = GeolocationPolicyDecision.allow obj_
  method deny = GeolocationPolicyDecision.deny obj_
end

class geolocation_policy_decision obj_ = object(self)
  method as_geolocationpolicydecision : GtkWebkitTypes.geolocationpolicydecision obj = obj_
  inherit geolocation_policy_decision_skel obj_
end



let wrap_geolocationpolicydecision obj = new geolocation_policy_decision (unsafe_cast obj)
let unwrap_geolocationpolicydecision obj = unsafe_cast obj#as_geolocationpolicydecision
let conv_geolocationpolicydecision =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_geolocationpolicydecision c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_geolocationpolicydecision c))) }

