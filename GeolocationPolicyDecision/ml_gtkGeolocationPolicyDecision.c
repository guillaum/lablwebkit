#include "ml_gtkWebkit.h"
#include "ml_gtkGeolocationPolicyDecision_tags_c.h"



ML_0(webkit_geolocation_policy_decision_get_type, Val_gtype)

ML_1(webkit_geolocation_policy_allow, WebkitGeolocationPolicyDecision_val, Unit)

ML_1(webkit_geolocation_policy_deny, WebkitGeolocationPolicyDecision_val, Unit)

