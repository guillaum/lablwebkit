open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module GeolocationPolicyDecision = struct
  let cast w : geolocationpolicydecision obj =
    try_cast w "WebkitGeolocationPolicyDecision"
  let create pl : geolocationpolicydecision obj =
    Gobject.unsafe_create "WebkitGeolocationPolicyDecision" pl
end

