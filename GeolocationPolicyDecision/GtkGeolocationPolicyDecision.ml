open Gobject
open Data
open GtkWebkitTypes
module GeolocationPolicyDecision = struct
  
  external allow : geolocationpolicydecision Gtk.obj -> unit = "ml_webkit_geolocation_policy_allow"
  external deny : geolocationpolicydecision Gtk.obj -> unit = "ml_webkit_geolocation_policy_deny"
end