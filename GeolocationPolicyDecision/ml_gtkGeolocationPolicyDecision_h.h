#include "GeolocationPolicyDecision/ml_gtkGeolocationPolicyDecision_tags_h.h"

#define WebkitGeolocationPolicyDecision_val(val) check_cast(WEBKIT_GEOLOCATION_POLICY_DECISION,val)
#define Val_webkit_geolocation_policy_decision(val) Val_GtkAny(val)
