open Gobject
open Data
open GtkWebkitTypes
module WebSettings = struct
  include GtkWebSettingsProps.WebSettings
  external create : unit -> websettings Gtk.obj = "ml_webkit_web_settings_new"
  external copy : websettings Gtk.obj -> websettings Gtk.obj = "ml_webkit_web_settings_copy"
end