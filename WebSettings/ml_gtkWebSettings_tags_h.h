/* webkit_editing_behavior : tags and macros */
#define MLTAG_UNIX	((value)(946508424*2+1))
#define MLTAG_WINDOWS	((value)(-397888925*2+1))
#define MLTAG_MAC	((value)(3843695*2+1))

extern const lookup_info ml_table_webkit_editing_behavior[];
#define Val_webkit_editing_behavior(data) ml_lookup_from_c (ml_table_webkit_editing_behavior, data)
#define Webkit_editing_behavior_val(key) ml_lookup_to_c (ml_table_webkit_editing_behavior, key)

