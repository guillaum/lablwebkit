open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebSettings = struct
  let cast w : websettings obj = try_cast w "WebkitWebSettings"
  module P = struct
    let default_encoding : ([>`websettings],_) property =
      {name="default-encoding"; conv=string}
    let cursive_font_family : ([>`websettings],_) property =
      {name="cursive-font-family"; conv=string}
    let default_font_family : ([>`websettings],_) property =
      {name="default-font-family"; conv=string}
    let fantasy_font_family : ([>`websettings],_) property =
      {name="fantasy-font-family"; conv=string}
    let monospace_font_family : ([>`websettings],_) property =
      {name="monospace-font-family"; conv=string}
    let sans_serif_font_family : ([>`websettings],_) property =
      {name="sans-serif-font-family"; conv=string}
    let serif_font_family : ([>`websettings],_) property =
      {name="serif-font-family"; conv=string}
    let default_font_size : ([>`websettings],_) property =
      {name="default-font-size"; conv=int}
    let default_monospace_font_size : ([>`websettings],_) property =
      {name="default-monospace-font-size"; conv=int}
    let minimum_font_size : ([>`websettings],_) property =
      {name="minimum-font-size"; conv=int}
    let minimum_logical_font_size : ([>`websettings],_) property =
      {name="minimum-logical-font-size"; conv=int}
    let enforce_96_dpi : ([>`websettings],_) property =
      {name="enforce-96-dpi"; conv=boolean}
    let auto_load_images : ([>`websettings],_) property =
      {name="auto-load-images"; conv=boolean}
    let auto_shrink_images : ([>`websettings],_) property =
      {name="auto-shrink-images"; conv=boolean}
    let print_backgrounds : ([>`websettings],_) property =
      {name="print-backgrounds"; conv=boolean}
    let enable_scripts : ([>`websettings],_) property =
      {name="enable-scripts"; conv=boolean}
    let enable_plugins : ([>`websettings],_) property =
      {name="enable-plugins"; conv=boolean}
    let resizable_text_areas : ([>`websettings],_) property =
      {name="resizable-text-areas"; conv=boolean}
    let user_stylesheet_uri : ([>`websettings],_) property =
      {name="user-stylesheet-uri"; conv=string}
    let zoom_step : ([>`websettings],_) property =
      {name="zoom-step"; conv=float}
    let enable_developer_extras : ([>`websettings],_) property =
      {name="enable-developer-extras"; conv=boolean}
    let enable_private_browsing : ([>`websettings],_) property =
      {name="enable-private-browsing"; conv=boolean}
    let enable_spell_checking : ([>`websettings],_) property =
      {name="enable-spell-checking"; conv=boolean}
    let spell_checking_languages : ([>`websettings],_) property =
      {name="spell-checking-languages"; conv=string}
    let enable_caret_browsing : ([>`websettings],_) property =
      {name="enable-caret-browsing"; conv=boolean}
    let enable_html5_database : ([>`websettings],_) property =
      {name="enable-html5-database"; conv=boolean}
    let enable_html5_local_storage : ([>`websettings],_) property =
      {name="enable-html5-local-storage"; conv=boolean}
    let html5_local_storage_database_path : ([>`websettings],_) property =
      {name="html5-local-storage-database-path"; conv=string}
    let enable_xss_auditor : ([>`websettings],_) property =
      {name="enable-xss-auditor"; conv=boolean}
    let enable_spatial_navigation : ([>`websettings],_) property =
      {name="enable-spatial-navigation"; conv=boolean}
    let enable_frame_flattening : ([>`websettings],_) property =
      {name="enable-frame-flattening"; conv=boolean}
    let user_agent : ([>`websettings],_) property =
      {name="user-agent"; conv=string}
    let javascript_can_open_windows_automatically : ([>`websettings],_) property =
      {name="javascript-can-open-windows-automatically"; conv=boolean}
    let javascript_can_access_clipboard : ([>`websettings],_) property =
      {name="javascript-can-access-clipboard"; conv=boolean}
    let enable_offline_web_application_cache : ([>`websettings],_) property =
      {name="enable-offline-web-application-cache"; conv=boolean}
    let editing_behavior : ([>`websettings],_) property =
      {name="editing-behavior"; conv=webkit_editing_behavior_conv}
    let enable_universal_access_from_file_uris : ([>`websettings],_) property =
      {name="enable-universal-access-from-file-uris"; conv=boolean}
    let enable_file_access_from_file_uris : ([>`websettings],_) property =
      {name="enable-file-access-from-file-uris"; conv=boolean}
    let enable_dom_paste : ([>`websettings],_) property =
      {name="enable-dom-paste"; conv=boolean}
    let tab_key_cycles_through_elements : ([>`websettings],_) property =
      {name="tab-key-cycles-through-elements"; conv=boolean}
    let enable_default_context_menu : ([>`websettings],_) property =
      {name="enable-default-context-menu"; conv=boolean}
    let enable_site_specific_quirks : ([>`websettings],_) property =
      {name="enable-site-specific-quirks"; conv=boolean}
    let enable_page_cache : ([>`websettings],_) property =
      {name="enable-page-cache"; conv=boolean}
    let auto_resize_window : ([>`websettings],_) property =
      {name="auto-resize-window"; conv=boolean}
    let enable_java_applet : ([>`websettings],_) property =
      {name="enable-java-applet"; conv=boolean}
    let enable_hyperlink_auditing : ([>`websettings],_) property =
      {name="enable-hyperlink-auditing"; conv=boolean}
    let enable_fullscreen : ([>`websettings],_) property =
      {name="enable-fullscreen"; conv=boolean}
    let enable_dns_prefetching : ([>`websettings],_) property =
      {name="enable-dns-prefetching"; conv=boolean}
    let enable_webgl : ([>`websettings],_) property =
      {name="enable-webgl"; conv=boolean}
    let enable_webaudio : ([>`websettings],_) property =
      {name="enable-webaudio"; conv=boolean}
    let enable_accelerated_compositing : ([>`websettings],_) property =
      {name="enable-accelerated-compositing"; conv=boolean}
  end
  let create pl : websettings obj =
    Gobject.unsafe_create "WebkitWebSettings" pl
  let make_params ~cont pl ?default_encoding ?cursive_font_family
      ?default_font_family ?fantasy_font_family ?monospace_font_family
      ?sans_serif_font_family ?serif_font_family ?default_font_size
      ?default_monospace_font_size ?minimum_font_size
      ?minimum_logical_font_size ?enforce_96_dpi ?auto_load_images
      ?auto_shrink_images ?print_backgrounds ?enable_scripts ?enable_plugins
      ?resizable_text_areas ?user_stylesheet_uri ?zoom_step
      ?enable_developer_extras ?enable_private_browsing
      ?enable_spell_checking ?spell_checking_languages ?enable_caret_browsing
      ?enable_html5_database ?enable_html5_local_storage
      ?html5_local_storage_database_path ?enable_xss_auditor
      ?enable_spatial_navigation ?enable_frame_flattening ?user_agent
      ?javascript_can_open_windows_automatically
      ?javascript_can_access_clipboard ?enable_offline_web_application_cache
      ?editing_behavior ?enable_universal_access_from_file_uris
      ?enable_file_access_from_file_uris ?enable_dom_paste
      ?tab_key_cycles_through_elements ?enable_default_context_menu
      ?enable_site_specific_quirks ?enable_page_cache ?auto_resize_window
      ?enable_java_applet ?enable_hyperlink_auditing ?enable_fullscreen
      ?enable_dns_prefetching ?enable_webgl ?enable_webaudio
      ?enable_accelerated_compositing =
    let pl = (
      may_cons P.default_encoding default_encoding (
      may_cons P.cursive_font_family cursive_font_family (
      may_cons P.default_font_family default_font_family (
      may_cons P.fantasy_font_family fantasy_font_family (
      may_cons P.monospace_font_family monospace_font_family (
      may_cons P.sans_serif_font_family sans_serif_font_family (
      may_cons P.serif_font_family serif_font_family (
      may_cons P.default_font_size default_font_size (
      may_cons P.default_monospace_font_size default_monospace_font_size (
      may_cons P.minimum_font_size minimum_font_size (
      may_cons P.minimum_logical_font_size minimum_logical_font_size (
      may_cons P.enforce_96_dpi enforce_96_dpi (
      may_cons P.auto_load_images auto_load_images (
      may_cons P.auto_shrink_images auto_shrink_images (
      may_cons P.print_backgrounds print_backgrounds (
      may_cons P.enable_scripts enable_scripts (
      may_cons P.enable_plugins enable_plugins (
      may_cons P.resizable_text_areas resizable_text_areas (
      may_cons P.user_stylesheet_uri user_stylesheet_uri (
      may_cons P.zoom_step zoom_step (
      may_cons P.enable_developer_extras enable_developer_extras (
      may_cons P.enable_private_browsing enable_private_browsing (
      may_cons P.enable_spell_checking enable_spell_checking (
      may_cons P.spell_checking_languages spell_checking_languages (
      may_cons P.enable_caret_browsing enable_caret_browsing (
      may_cons P.enable_html5_database enable_html5_database (
      may_cons P.enable_html5_local_storage enable_html5_local_storage (
      may_cons P.html5_local_storage_database_path html5_local_storage_database_path (
      may_cons P.enable_xss_auditor enable_xss_auditor (
      may_cons P.enable_spatial_navigation enable_spatial_navigation (
      may_cons P.enable_frame_flattening enable_frame_flattening (
      may_cons P.user_agent user_agent (
      may_cons P.javascript_can_open_windows_automatically javascript_can_open_windows_automatically (
      may_cons P.javascript_can_access_clipboard javascript_can_access_clipboard (
      may_cons P.enable_offline_web_application_cache enable_offline_web_application_cache (
      may_cons P.editing_behavior editing_behavior (
      may_cons P.enable_universal_access_from_file_uris enable_universal_access_from_file_uris (
      may_cons P.enable_file_access_from_file_uris enable_file_access_from_file_uris (
      may_cons P.enable_dom_paste enable_dom_paste (
      may_cons P.tab_key_cycles_through_elements tab_key_cycles_through_elements (
      may_cons P.enable_default_context_menu enable_default_context_menu (
      may_cons P.enable_site_specific_quirks enable_site_specific_quirks (
      may_cons P.enable_page_cache enable_page_cache (
      may_cons P.auto_resize_window auto_resize_window (
      may_cons P.enable_java_applet enable_java_applet (
      may_cons P.enable_hyperlink_auditing enable_hyperlink_auditing (
      may_cons P.enable_fullscreen enable_fullscreen (
      may_cons P.enable_dns_prefetching enable_dns_prefetching (
      may_cons P.enable_webgl enable_webgl (
      may_cons P.enable_webaudio enable_webaudio (
      may_cons P.enable_accelerated_compositing enable_accelerated_compositing pl))))))))))))))))))))))))))))))))))))))))))))))))))) in
    cont pl
end

