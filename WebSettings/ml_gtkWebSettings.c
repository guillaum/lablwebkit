#include "ml_gtkWebkit.h"
#include "ml_gtkWebSettings_tags_c.h"

#define WebkitEditingBehavior_val Webkit_editing_behavior_val

ML_0(webkit_web_settings_get_type, Val_gtype)

ML_0(webkit_web_settings_new, Val_webkit_web_settings)

ML_1(webkit_web_settings_copy, WebkitWebSettings_val, Val_webkit_web_settings)

ML_1(webkit_web_settings_get_user_agent, WebkitWebSettings_val, Val_string)

