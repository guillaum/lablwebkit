open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebSettingsProps

class virtual web_settings_props = object
  val virtual obj : _ obj
  method set_default_encoding = set WebSettings.P.default_encoding obj
  method set_cursive_font_family = set WebSettings.P.cursive_font_family obj
  method set_default_font_family = set WebSettings.P.default_font_family obj
  method set_fantasy_font_family = set WebSettings.P.fantasy_font_family obj
  method set_monospace_font_family =
    set WebSettings.P.monospace_font_family obj
  method set_sans_serif_font_family =
    set WebSettings.P.sans_serif_font_family obj
  method set_serif_font_family = set WebSettings.P.serif_font_family obj
  method set_default_font_size = set WebSettings.P.default_font_size obj
  method set_default_monospace_font_size =
    set WebSettings.P.default_monospace_font_size obj
  method set_minimum_font_size = set WebSettings.P.minimum_font_size obj
  method set_minimum_logical_font_size =
    set WebSettings.P.minimum_logical_font_size obj
  method set_enforce_96_dpi = set WebSettings.P.enforce_96_dpi obj
  method set_auto_load_images = set WebSettings.P.auto_load_images obj
  method set_auto_shrink_images = set WebSettings.P.auto_shrink_images obj
  method set_print_backgrounds = set WebSettings.P.print_backgrounds obj
  method set_enable_scripts = set WebSettings.P.enable_scripts obj
  method set_enable_plugins = set WebSettings.P.enable_plugins obj
  method set_resizable_text_areas =
    set WebSettings.P.resizable_text_areas obj
  method set_user_stylesheet_uri = set WebSettings.P.user_stylesheet_uri obj
  method set_zoom_step = set WebSettings.P.zoom_step obj
  method set_enable_developer_extras =
    set WebSettings.P.enable_developer_extras obj
  method set_enable_private_browsing =
    set WebSettings.P.enable_private_browsing obj
  method set_enable_spell_checking =
    set WebSettings.P.enable_spell_checking obj
  method set_spell_checking_languages =
    set WebSettings.P.spell_checking_languages obj
  method set_enable_caret_browsing =
    set WebSettings.P.enable_caret_browsing obj
  method set_enable_html5_database =
    set WebSettings.P.enable_html5_database obj
  method set_enable_html5_local_storage =
    set WebSettings.P.enable_html5_local_storage obj
  method set_html5_local_storage_database_path =
    set WebSettings.P.html5_local_storage_database_path obj
  method set_enable_xss_auditor = set WebSettings.P.enable_xss_auditor obj
  method set_enable_spatial_navigation =
    set WebSettings.P.enable_spatial_navigation obj
  method set_enable_frame_flattening =
    set WebSettings.P.enable_frame_flattening obj
  method set_user_agent = set WebSettings.P.user_agent obj
  method set_javascript_can_open_windows_automatically =
    set WebSettings.P.javascript_can_open_windows_automatically obj
  method set_javascript_can_access_clipboard =
    set WebSettings.P.javascript_can_access_clipboard obj
  method set_enable_offline_web_application_cache =
    set WebSettings.P.enable_offline_web_application_cache obj
  method set_editing_behavior = set WebSettings.P.editing_behavior obj
  method set_enable_universal_access_from_file_uris =
    set WebSettings.P.enable_universal_access_from_file_uris obj
  method set_enable_file_access_from_file_uris =
    set WebSettings.P.enable_file_access_from_file_uris obj
  method set_enable_dom_paste = set WebSettings.P.enable_dom_paste obj
  method set_tab_key_cycles_through_elements =
    set WebSettings.P.tab_key_cycles_through_elements obj
  method set_enable_default_context_menu =
    set WebSettings.P.enable_default_context_menu obj
  method set_enable_site_specific_quirks =
    set WebSettings.P.enable_site_specific_quirks obj
  method set_enable_page_cache = set WebSettings.P.enable_page_cache obj
  method set_auto_resize_window = set WebSettings.P.auto_resize_window obj
  method set_enable_java_applet = set WebSettings.P.enable_java_applet obj
  method set_enable_hyperlink_auditing =
    set WebSettings.P.enable_hyperlink_auditing obj
  method set_enable_fullscreen = set WebSettings.P.enable_fullscreen obj
  method set_enable_dns_prefetching =
    set WebSettings.P.enable_dns_prefetching obj
  method set_enable_webgl = set WebSettings.P.enable_webgl obj
  method set_enable_webaudio = set WebSettings.P.enable_webaudio obj
  method set_enable_accelerated_compositing =
    set WebSettings.P.enable_accelerated_compositing obj
  method default_encoding = get WebSettings.P.default_encoding obj
  method cursive_font_family = get WebSettings.P.cursive_font_family obj
  method default_font_family = get WebSettings.P.default_font_family obj
  method fantasy_font_family = get WebSettings.P.fantasy_font_family obj
  method monospace_font_family = get WebSettings.P.monospace_font_family obj
  method sans_serif_font_family =
    get WebSettings.P.sans_serif_font_family obj
  method serif_font_family = get WebSettings.P.serif_font_family obj
  method default_font_size = get WebSettings.P.default_font_size obj
  method default_monospace_font_size =
    get WebSettings.P.default_monospace_font_size obj
  method minimum_font_size = get WebSettings.P.minimum_font_size obj
  method minimum_logical_font_size =
    get WebSettings.P.minimum_logical_font_size obj
  method enforce_96_dpi = get WebSettings.P.enforce_96_dpi obj
  method auto_load_images = get WebSettings.P.auto_load_images obj
  method auto_shrink_images = get WebSettings.P.auto_shrink_images obj
  method print_backgrounds = get WebSettings.P.print_backgrounds obj
  method enable_scripts = get WebSettings.P.enable_scripts obj
  method enable_plugins = get WebSettings.P.enable_plugins obj
  method resizable_text_areas = get WebSettings.P.resizable_text_areas obj
  method user_stylesheet_uri = get WebSettings.P.user_stylesheet_uri obj
  method zoom_step = get WebSettings.P.zoom_step obj
  method enable_developer_extras =
    get WebSettings.P.enable_developer_extras obj
  method enable_private_browsing =
    get WebSettings.P.enable_private_browsing obj
  method enable_spell_checking = get WebSettings.P.enable_spell_checking obj
  method spell_checking_languages =
    get WebSettings.P.spell_checking_languages obj
  method enable_caret_browsing = get WebSettings.P.enable_caret_browsing obj
  method enable_html5_database = get WebSettings.P.enable_html5_database obj
  method enable_html5_local_storage =
    get WebSettings.P.enable_html5_local_storage obj
  method html5_local_storage_database_path =
    get WebSettings.P.html5_local_storage_database_path obj
  method enable_xss_auditor = get WebSettings.P.enable_xss_auditor obj
  method enable_spatial_navigation =
    get WebSettings.P.enable_spatial_navigation obj
  method enable_frame_flattening =
    get WebSettings.P.enable_frame_flattening obj
  method user_agent = get WebSettings.P.user_agent obj
  method javascript_can_open_windows_automatically =
    get WebSettings.P.javascript_can_open_windows_automatically obj
  method javascript_can_access_clipboard =
    get WebSettings.P.javascript_can_access_clipboard obj
  method enable_offline_web_application_cache =
    get WebSettings.P.enable_offline_web_application_cache obj
  method editing_behavior = get WebSettings.P.editing_behavior obj
  method enable_universal_access_from_file_uris =
    get WebSettings.P.enable_universal_access_from_file_uris obj
  method enable_file_access_from_file_uris =
    get WebSettings.P.enable_file_access_from_file_uris obj
  method enable_dom_paste = get WebSettings.P.enable_dom_paste obj
  method tab_key_cycles_through_elements =
    get WebSettings.P.tab_key_cycles_through_elements obj
  method enable_default_context_menu =
    get WebSettings.P.enable_default_context_menu obj
  method enable_site_specific_quirks =
    get WebSettings.P.enable_site_specific_quirks obj
  method enable_page_cache = get WebSettings.P.enable_page_cache obj
  method auto_resize_window = get WebSettings.P.auto_resize_window obj
  method enable_java_applet = get WebSettings.P.enable_java_applet obj
  method enable_hyperlink_auditing =
    get WebSettings.P.enable_hyperlink_auditing obj
  method enable_fullscreen = get WebSettings.P.enable_fullscreen obj
  method enable_dns_prefetching =
    get WebSettings.P.enable_dns_prefetching obj
  method enable_webgl = get WebSettings.P.enable_webgl obj
  method enable_webaudio = get WebSettings.P.enable_webaudio obj
  method enable_accelerated_compositing =
    get WebSettings.P.enable_accelerated_compositing obj
end

class virtual web_settings_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method default_encoding = self#notify WebSettings.P.default_encoding
  method cursive_font_family = self#notify WebSettings.P.cursive_font_family
  method default_font_family = self#notify WebSettings.P.default_font_family
  method fantasy_font_family = self#notify WebSettings.P.fantasy_font_family
  method monospace_font_family =
    self#notify WebSettings.P.monospace_font_family
  method sans_serif_font_family =
    self#notify WebSettings.P.sans_serif_font_family
  method serif_font_family = self#notify WebSettings.P.serif_font_family
  method default_font_size = self#notify WebSettings.P.default_font_size
  method default_monospace_font_size =
    self#notify WebSettings.P.default_monospace_font_size
  method minimum_font_size = self#notify WebSettings.P.minimum_font_size
  method minimum_logical_font_size =
    self#notify WebSettings.P.minimum_logical_font_size
  method enforce_96_dpi = self#notify WebSettings.P.enforce_96_dpi
  method auto_load_images = self#notify WebSettings.P.auto_load_images
  method auto_shrink_images = self#notify WebSettings.P.auto_shrink_images
  method print_backgrounds = self#notify WebSettings.P.print_backgrounds
  method enable_scripts = self#notify WebSettings.P.enable_scripts
  method enable_plugins = self#notify WebSettings.P.enable_plugins
  method resizable_text_areas =
    self#notify WebSettings.P.resizable_text_areas
  method user_stylesheet_uri = self#notify WebSettings.P.user_stylesheet_uri
  method zoom_step = self#notify WebSettings.P.zoom_step
  method enable_developer_extras =
    self#notify WebSettings.P.enable_developer_extras
  method enable_private_browsing =
    self#notify WebSettings.P.enable_private_browsing
  method enable_spell_checking =
    self#notify WebSettings.P.enable_spell_checking
  method spell_checking_languages =
    self#notify WebSettings.P.spell_checking_languages
  method enable_caret_browsing =
    self#notify WebSettings.P.enable_caret_browsing
  method enable_html5_database =
    self#notify WebSettings.P.enable_html5_database
  method enable_html5_local_storage =
    self#notify WebSettings.P.enable_html5_local_storage
  method html5_local_storage_database_path =
    self#notify WebSettings.P.html5_local_storage_database_path
  method enable_xss_auditor = self#notify WebSettings.P.enable_xss_auditor
  method enable_spatial_navigation =
    self#notify WebSettings.P.enable_spatial_navigation
  method enable_frame_flattening =
    self#notify WebSettings.P.enable_frame_flattening
  method user_agent = self#notify WebSettings.P.user_agent
  method javascript_can_open_windows_automatically =
    self#notify WebSettings.P.javascript_can_open_windows_automatically
  method javascript_can_access_clipboard =
    self#notify WebSettings.P.javascript_can_access_clipboard
  method enable_offline_web_application_cache =
    self#notify WebSettings.P.enable_offline_web_application_cache
  method editing_behavior = self#notify WebSettings.P.editing_behavior
  method enable_universal_access_from_file_uris =
    self#notify WebSettings.P.enable_universal_access_from_file_uris
  method enable_file_access_from_file_uris =
    self#notify WebSettings.P.enable_file_access_from_file_uris
  method enable_dom_paste = self#notify WebSettings.P.enable_dom_paste
  method tab_key_cycles_through_elements =
    self#notify WebSettings.P.tab_key_cycles_through_elements
  method enable_default_context_menu =
    self#notify WebSettings.P.enable_default_context_menu
  method enable_site_specific_quirks =
    self#notify WebSettings.P.enable_site_specific_quirks
  method enable_page_cache = self#notify WebSettings.P.enable_page_cache
  method auto_resize_window = self#notify WebSettings.P.auto_resize_window
  method enable_java_applet = self#notify WebSettings.P.enable_java_applet
  method enable_hyperlink_auditing =
    self#notify WebSettings.P.enable_hyperlink_auditing
  method enable_fullscreen = self#notify WebSettings.P.enable_fullscreen
  method enable_dns_prefetching =
    self#notify WebSettings.P.enable_dns_prefetching
  method enable_webgl = self#notify WebSettings.P.enable_webgl
  method enable_webaudio = self#notify WebSettings.P.enable_webaudio
  method enable_accelerated_compositing =
    self#notify WebSettings.P.enable_accelerated_compositing
end

