(** webkit_web_settings enums *)

open Gpointer

type webkit_editing_behavior = [ `UNIX | `WINDOWS | `MAC ]

(**/**)

external _get_tables : unit ->
    webkit_editing_behavior variant_table
  = "ml_webkit_web_settings_get_tables"


let webkit_editing_behavior = _get_tables ()

let webkit_editing_behavior_conv = Gobject.Data.enum webkit_editing_behavior
