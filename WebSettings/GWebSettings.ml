open GObj
open Gobject
open Gtk
open GtkWebSettings
open OGtkWebSettingsProps

class web_settings_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_settings_props
  method copy = WebSettings.copy obj_
end

class web_settings obj_ = object(self)
  method as_websettings : GtkWebkitTypes.websettings obj = obj_
  inherit web_settings_skel obj_
end

let web_settings () =
  new web_settings (WebSettings.create ())

let wrap_websettings obj = new web_settings (unsafe_cast obj)
let unwrap_websettings obj = unsafe_cast obj#as_websettings
let conv_websettings =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_websettings c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_websettings c))) }

