/* webkit_editing_behavior : conversion table */
const lookup_info ml_table_webkit_editing_behavior[] = {
  { 0, 3 },
  { MLTAG_WINDOWS, WEBKIT_EDITING_BEHAVIOR_WINDOWS },
  { MLTAG_MAC, WEBKIT_EDITING_BEHAVIOR_MAC },
  { MLTAG_UNIX, WEBKIT_EDITING_BEHAVIOR_UNIX },
};

CAMLprim value ml_webkit_web_settings_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_editing_behavior,
  };
  return (value)ml_lookup_tables[0];}
