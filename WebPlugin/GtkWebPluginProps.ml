open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebPlugin = struct
  let cast w : webplugin obj = try_cast w "WebkitWebPlugin"
  module P = struct
    let enabled : ([>`webplugin],_) property = {name="enabled"; conv=boolean}
  end
  let create pl : webplugin obj = Gobject.unsafe_create "WebkitWebPlugin" pl
  let make_params ~cont pl ?enabled =
    let pl = (may_cons P.enabled enabled pl) in
    cont pl
end

