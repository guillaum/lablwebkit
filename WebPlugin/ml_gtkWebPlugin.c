#include "ml_gtkWebkit.h"
#include "ml_gtkWebPlugin_tags_c.h"



ML_0(webkit_web_plugin_get_type, Val_gtype)

ML_1(webkit_web_plugin_get_name, WebkitWebPlugin_val, Val_char)

ML_1(webkit_web_plugin_get_description, WebkitWebPlugin_val, Val_char)

ML_1(webkit_web_plugin_get_path, WebkitWebPlugin_val, Val_char)

ML_2(webkit_web_plugin_set_enabled, WebkitWebPlugin_val, Bool_val, Unit)

ML_1(webkit_web_plugin_get_enabled, WebkitWebPlugin_val, Val_bool)

