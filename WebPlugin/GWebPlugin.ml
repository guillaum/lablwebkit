open GObj
open Gobject
open Gtk
open GtkWebPlugin
open OGtkWebPluginProps

class web_plugin_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_plugin_props
  method get_name = WebPlugin.get_name obj_
  method get_description = WebPlugin.get_description obj_
  method get_path = WebPlugin.get_path obj_
end

class web_plugin obj_ = object(self)
  method as_webplugin : GtkWebkitTypes.webplugin obj = obj_
  inherit web_plugin_skel obj_
end



let wrap_webplugin obj = new web_plugin (unsafe_cast obj)
let unwrap_webplugin obj = unsafe_cast obj#as_webplugin
let conv_webplugin =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webplugin c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webplugin c))) }

