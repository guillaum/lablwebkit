open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebPluginProps

class virtual web_plugin_props = object
  val virtual obj : _ obj
  method set_enabled = set WebPlugin.P.enabled obj
  method enabled = get WebPlugin.P.enabled obj
end

class virtual web_plugin_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method enabled = self#notify WebPlugin.P.enabled
end

