open Gobject
open Data
open GtkWebkitTypes
module WebPlugin = struct
  include GtkWebPluginProps.WebPlugin
  
  external get_name : webplugin Gtk.obj -> char = "ml_webkit_web_plugin_get_name"
  external get_description : webplugin Gtk.obj -> char = "ml_webkit_web_plugin_get_description"
  external get_path : webplugin Gtk.obj -> char = "ml_webkit_web_plugin_get_path"
end