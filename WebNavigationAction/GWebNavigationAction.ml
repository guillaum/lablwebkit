open GObj
open Gobject
open Gtk
open GtkWebNavigationAction
open OGtkWebNavigationActionProps

class web_navigation_action_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_navigation_action_props
end

class web_navigation_action obj_ = object(self)
  method as_webnavigationaction : GtkWebkitTypes.webnavigationaction obj = obj_
  inherit web_navigation_action_skel obj_
end



let wrap_webnavigationaction obj = new web_navigation_action (unsafe_cast obj)
let unwrap_webnavigationaction obj = unsafe_cast obj#as_webnavigationaction
let conv_webnavigationaction =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webnavigationaction c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webnavigationaction c))) }

