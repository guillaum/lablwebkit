/* webkit_web_navigation_reason : tags and macros */
#define MLTAG_OTHER	((value)(879009456*2+1))
#define MLTAG_FORM_RESUBMITTED	((value)(-324736051*2+1))
#define MLTAG_RELOAD	((value)(912392025*2+1))
#define MLTAG_BACK_FORWARD	((value)(-345919411*2+1))
#define MLTAG_FORM_SUBMITTED	((value)(330561184*2+1))
#define MLTAG_LINK_CLICKED	((value)(-614199806*2+1))

extern const lookup_info ml_table_webkit_web_navigation_reason[];
#define Val_webkit_web_navigation_reason(data) ml_lookup_from_c (ml_table_webkit_web_navigation_reason, data)
#define Webkit_web_navigation_reason_val(key) ml_lookup_to_c (ml_table_webkit_web_navigation_reason, key)

