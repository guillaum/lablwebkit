open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebNavigationActionProps

class virtual web_navigation_action_props = object
  val virtual obj : _ obj
  method set_reason = set WebNavigationAction.P.reason obj
  method set_original_uri = set WebNavigationAction.P.original_uri obj
  method reason = get WebNavigationAction.P.reason obj
  method original_uri = get WebNavigationAction.P.original_uri obj
  method button = get WebNavigationAction.P.button obj
  method modifier_state = get WebNavigationAction.P.modifier_state obj
  method target_frame = get WebNavigationAction.P.target_frame obj
end

class virtual web_navigation_action_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method reason = self#notify WebNavigationAction.P.reason
  method original_uri = self#notify WebNavigationAction.P.original_uri
  method button = self#notify WebNavigationAction.P.button
  method modifier_state = self#notify WebNavigationAction.P.modifier_state
  method target_frame = self#notify WebNavigationAction.P.target_frame
end

