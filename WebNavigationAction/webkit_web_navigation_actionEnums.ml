(** webkit_web_navigation_action enums *)

open Gpointer

type webkit_web_navigation_reason =
  [ `OTHER | `FORM_RESUBMITTED | `RELOAD | `BACK_FORWARD | `FORM_SUBMITTED
  | `LINK_CLICKED ]

(**/**)

external _get_tables : unit ->
    webkit_web_navigation_reason variant_table
  = "ml_webkit_web_navigation_action_get_tables"


let webkit_web_navigation_reason = _get_tables ()

let webkit_web_navigation_reason_conv = Gobject.Data.enum webkit_web_navigation_reason
