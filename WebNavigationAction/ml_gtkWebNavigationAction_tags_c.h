/* webkit_web_navigation_reason : conversion table */
const lookup_info ml_table_webkit_web_navigation_reason[] = {
  { 0, 6 },
  { MLTAG_LINK_CLICKED, WEBKIT_WEB_NAVIGATION_REASON_LINK_CLICKED },
  { MLTAG_BACK_FORWARD, WEBKIT_WEB_NAVIGATION_REASON_BACK_FORWARD },
  { MLTAG_FORM_RESUBMITTED, WEBKIT_WEB_NAVIGATION_REASON_FORM_RESUBMITTED },
  { MLTAG_FORM_SUBMITTED, WEBKIT_WEB_NAVIGATION_REASON_FORM_SUBMITTED },
  { MLTAG_OTHER, WEBKIT_WEB_NAVIGATION_REASON_OTHER },
  { MLTAG_RELOAD, WEBKIT_WEB_NAVIGATION_REASON_RELOAD },
};

CAMLprim value ml_webkit_web_navigation_action_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_web_navigation_reason,
  };
  return (value)ml_lookup_tables[0];}
