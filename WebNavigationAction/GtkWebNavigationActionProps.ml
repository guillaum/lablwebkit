open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebNavigationAction = struct
  let cast w : webnavigationaction obj =
    try_cast w "WebkitWebNavigationAction"
  module P = struct
    let reason : ([>`webnavigationaction],_) property =
      {name="reason"; conv=webkit_web_navigation_reason_conv}
    let original_uri : ([>`webnavigationaction],_) property =
      {name="original-uri"; conv=string}
    let button : ([>`webnavigationaction],_) property =
      {name="button"; conv=int}
    let modifier_state : ([>`webnavigationaction],_) property =
      {name="modifier-state"; conv=int}
    let target_frame : ([>`webnavigationaction],_) property =
      {name="target-frame"; conv=string}
  end
  let create ?button ?modifier_state ?target_frame pl : webnavigationaction obj =
    let pl = (
      may_cons P.button button (
      may_cons P.modifier_state modifier_state (
      may_cons P.target_frame target_frame pl))) in
    Gobject.unsafe_create "WebkitWebNavigationAction" pl
  let make_params ~cont pl ?reason ?original_uri =
    let pl = (
      may_cons P.reason reason (
      may_cons P.original_uri original_uri pl)) in
    cont pl
end

