open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebPolicyDecision = struct
  let cast w : webpolicydecision obj = try_cast w "WebkitWebPolicyDecision"
  let create pl : webpolicydecision obj =
    Gobject.unsafe_create "WebkitWebPolicyDecision" pl
end

