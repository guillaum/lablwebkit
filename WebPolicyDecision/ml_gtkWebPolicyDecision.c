#include "ml_gtkWebkit.h"
#include "ml_gtkWebPolicyDecision_tags_c.h"



ML_0(webkit_web_policy_decision_get_type, Val_gtype)

ML_1(webkit_web_policy_decision_use, WebkitWebPolicyDecision_val, Unit)

ML_1(webkit_web_policy_decision_ignore, WebkitWebPolicyDecision_val, Unit)

ML_1(webkit_web_policy_decision_download, WebkitWebPolicyDecision_val, Unit)

