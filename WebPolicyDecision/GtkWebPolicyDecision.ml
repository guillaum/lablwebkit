open Gobject
open Data
open GtkWebkitTypes
module WebPolicyDecision = struct
  
  external use : webpolicydecision Gtk.obj -> unit = "ml_webkit_web_policy_decision_use"
  external ignore : webpolicydecision Gtk.obj -> unit = "ml_webkit_web_policy_decision_ignore"
  external download : webpolicydecision Gtk.obj -> unit = "ml_webkit_web_policy_decision_download"
end