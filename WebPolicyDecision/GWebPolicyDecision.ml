open GObj
open Gobject
open Gtk
open GtkWebPolicyDecision
open OGtkWebPolicyDecisionProps

class web_policy_decision_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  method use = WebPolicyDecision.use obj_
  method ignore = WebPolicyDecision.ignore obj_
  method download = WebPolicyDecision.download obj_
end

class web_policy_decision obj_ = object(self)
  method as_webpolicydecision : GtkWebkitTypes.webpolicydecision obj = obj_
  inherit web_policy_decision_skel obj_
end



let wrap_webpolicydecision obj = new web_policy_decision (unsafe_cast obj)
let unwrap_webpolicydecision obj = unsafe_cast obj#as_webpolicydecision
let conv_webpolicydecision =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webpolicydecision c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webpolicydecision c))) }

