#include "Version/ml_gtkVersion_tags_h.h"

#define WebkitVersion_val(val) check_cast(WEBKIT_VERSION,val)
#define Val_webkit_version(val) Val_GtkAny(val)
