#include "ml_gtkWebkit.h"
#include "ml_gtkVersion_tags_c.h"



ML_0(webkit_major_version, Val_uint)

ML_0(webkit_minor_version, Val_uint)

ML_0(webkit_micro_version, Val_uint)

ML_3(webkit_check_version, Uint_val, Uint_val, Uint_val, Val_bool)

