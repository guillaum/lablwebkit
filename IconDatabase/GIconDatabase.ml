open GObj
open Gobject
open Gtk
open GtkIconDatabase
open OGtkIconDatabaseProps

class icon_database_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit icon_database_props
  method get_icon_uri = IconDatabase.get_icon_uri obj_
  method clear = IconDatabase.clear obj_
end

class icon_database_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit icon_database_sigs
end

class icon_database obj_ = object(self)
  method as_icondatabase : GtkWebkitTypes.icondatabase obj = obj_
  inherit icon_database_skel obj_
  method connect = new icon_database_signals obj_
end



let wrap_icondatabase obj = new icon_database (unsafe_cast obj)
let unwrap_icondatabase obj = unsafe_cast obj#as_icondatabase
let conv_icondatabase =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_icondatabase c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_icondatabase c))) }

