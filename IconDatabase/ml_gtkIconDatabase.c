#include "ml_gtkWebkit.h"
#include "ml_gtkIconDatabase_tags_c.h"



ML_0(webkit_icon_database_get_type, Val_gtype)

ML_1(webkit_icon_database_get_path, WebkitIconDatabase_val, Val_string)

ML_2(webkit_icon_database_set_path, WebkitIconDatabase_val, String_val, Unit)

ML_2(webkit_icon_database_get_icon_uri, WebkitIconDatabase_val, String_val, Val_string)

ML_1(webkit_icon_database_clear, WebkitIconDatabase_val, Unit)

