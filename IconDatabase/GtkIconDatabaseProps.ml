open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module IconDatabase = struct
  let cast w : icondatabase obj = try_cast w "WebkitIconDatabase"
  module P = struct
    let path : ([>`icondatabase],_) property = {name="path"; conv=string}
  end
  module S = struct
    open GtkSignal
    let icon_loaded =
      {name="icon_loaded"; classe=`icondatabase; marshaller=fun f ->
       marshal2 GWebFrame.conv_webframe string
         "WebkitIconDatabase::icon_loaded" f}
  end
  let create pl : icondatabase obj =
    Gobject.unsafe_create "WebkitIconDatabase" pl
  let make_params ~cont pl ?path =
    let pl = (may_cons P.path path pl) in
    cont pl
end

