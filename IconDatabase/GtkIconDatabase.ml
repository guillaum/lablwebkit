open Gobject
open Data
open GtkWebkitTypes
module IconDatabase = struct
  include GtkIconDatabaseProps.IconDatabase
  
  external get_icon_uri : icondatabase Gtk.obj -> string -> string = "ml_webkit_icon_database_get_icon_uri"
  external clear : icondatabase Gtk.obj -> unit = "ml_webkit_icon_database_clear"
end