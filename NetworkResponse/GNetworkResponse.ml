open GObj
open Gobject
open Gtk
open GtkNetworkResponse
open OGtkNetworkResponseProps

class network_response_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit network_response_props
end

class network_response obj_ = object(self)
  method as_networkresponse : GtkWebkitTypes.networkresponse obj = obj_
  inherit network_response_skel obj_
end

let network_response uri =
  new network_response (NetworkResponse.create uri)

let wrap_networkresponse obj = new network_response (unsafe_cast obj)
let unwrap_networkresponse obj = unsafe_cast obj#as_networkresponse
let conv_networkresponse =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_networkresponse c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_networkresponse c))) }

