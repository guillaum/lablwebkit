#include "ml_gtkWebkit.h"
#include "ml_gtkNetworkResponse_tags_c.h"



ML_0(webkit_network_response_get_type, Val_gtype)

ML_1(webkit_network_response_new, String_val, Val_webkit_network_response)

ML_2(webkit_network_response_set_uri, WebkitNetworkResponse_val, String_val, Unit)

ML_1(webkit_network_response_get_uri, WebkitNetworkResponse_val, Val_string)

