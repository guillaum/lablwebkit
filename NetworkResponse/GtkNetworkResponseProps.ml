open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module NetworkResponse = struct
  let cast w : networkresponse obj = try_cast w "WebkitNetworkResponse"
  module P = struct
    let uri : ([>`networkresponse],_) property = {name="uri"; conv=string}
  end
  let create pl : networkresponse obj =
    Gobject.unsafe_create "WebkitNetworkResponse" pl
  let make_params ~cont pl ?uri = let pl = (may_cons P.uri uri pl) in cont pl
end

