open Gobject
open Data
open GtkWebkitTypes
module NetworkResponse = struct
  include GtkNetworkResponseProps.NetworkResponse
  external create : string -> networkresponse Gtk.obj = "ml_webkit_network_response_new"
  
end