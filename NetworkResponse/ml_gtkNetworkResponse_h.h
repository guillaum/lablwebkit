#include "NetworkResponse/ml_gtkNetworkResponse_tags_h.h"

#define WebkitNetworkResponse_val(val) check_cast(WEBKIT_NETWORK_RESPONSE,val)
#define Val_webkit_network_response(val) Val_GtkAny(val)
