open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkNetworkResponseProps

class virtual network_response_props = object
  val virtual obj : _ obj
  method set_uri = set NetworkResponse.P.uri obj
  method uri = get NetworkResponse.P.uri obj
end

class virtual network_response_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method uri = self#notify NetworkResponse.P.uri
end

