open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module ViewportAttributes = struct
  let cast w : viewportattributes obj = try_cast w "WebkitViewportAttributes"
  module P = struct
    let device_width : ([>`viewportattributes],_) property =
      {name="device-width"; conv=int}
    let device_height : ([>`viewportattributes],_) property =
      {name="device-height"; conv=int}
    let available_width : ([>`viewportattributes],_) property =
      {name="available-width"; conv=int}
    let available_height : ([>`viewportattributes],_) property =
      {name="available-height"; conv=int}
    let desktop_width : ([>`viewportattributes],_) property =
      {name="desktop-width"; conv=int}
    let device_dpi : ([>`viewportattributes],_) property =
      {name="device-dpi"; conv=int}
    let width : ([>`viewportattributes],_) property =
      {name="width"; conv=int}
    let height : ([>`viewportattributes],_) property =
      {name="height"; conv=int}
    let initial_scale_factor : ([>`viewportattributes],_) property =
      {name="initial-scale-factor"; conv=float}
    let minimum_scale_factor : ([>`viewportattributes],_) property =
      {name="minimum-scale-factor"; conv=float}
    let maximum_scale_factor : ([>`viewportattributes],_) property =
      {name="maximum-scale-factor"; conv=float}
    let device_pixel_ratio : ([>`viewportattributes],_) property =
      {name="device-pixel-ratio"; conv=float}
    let user_scalable : ([>`viewportattributes],_) property =
      {name="user-scalable"; conv=boolean}
    let valid : ([>`viewportattributes],_) property =
      {name="valid"; conv=boolean}
  end
  let create pl : viewportattributes obj =
    Gobject.unsafe_create "WebkitViewportAttributes" pl
  let make_params ~cont pl ?device_width ?device_height ?available_width
      ?available_height ?desktop_width ?device_dpi =
    let pl = (
      may_cons P.device_width device_width (
      may_cons P.device_height device_height (
      may_cons P.available_width available_width (
      may_cons P.available_height available_height (
      may_cons P.desktop_width desktop_width (
      may_cons P.device_dpi device_dpi pl)))))) in
    cont pl
end

