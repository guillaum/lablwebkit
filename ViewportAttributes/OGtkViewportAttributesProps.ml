open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkViewportAttributesProps

class virtual viewport_attributes_props = object
  val virtual obj : _ obj
  method set_device_width = set ViewportAttributes.P.device_width obj
  method set_device_height = set ViewportAttributes.P.device_height obj
  method set_available_width = set ViewportAttributes.P.available_width obj
  method set_available_height = set ViewportAttributes.P.available_height obj
  method set_desktop_width = set ViewportAttributes.P.desktop_width obj
  method set_device_dpi = set ViewportAttributes.P.device_dpi obj
  method device_width = get ViewportAttributes.P.device_width obj
  method device_height = get ViewportAttributes.P.device_height obj
  method available_width = get ViewportAttributes.P.available_width obj
  method available_height = get ViewportAttributes.P.available_height obj
  method desktop_width = get ViewportAttributes.P.desktop_width obj
  method device_dpi = get ViewportAttributes.P.device_dpi obj
  method width = get ViewportAttributes.P.width obj
  method height = get ViewportAttributes.P.height obj
  method initial_scale_factor =
    get ViewportAttributes.P.initial_scale_factor obj
  method minimum_scale_factor =
    get ViewportAttributes.P.minimum_scale_factor obj
  method maximum_scale_factor =
    get ViewportAttributes.P.maximum_scale_factor obj
  method device_pixel_ratio = get ViewportAttributes.P.device_pixel_ratio obj
  method user_scalable = get ViewportAttributes.P.user_scalable obj
  method valid = get ViewportAttributes.P.valid obj
end

class virtual viewport_attributes_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method device_width = self#notify ViewportAttributes.P.device_width
  method device_height = self#notify ViewportAttributes.P.device_height
  method available_width = self#notify ViewportAttributes.P.available_width
  method available_height = self#notify ViewportAttributes.P.available_height
  method desktop_width = self#notify ViewportAttributes.P.desktop_width
  method device_dpi = self#notify ViewportAttributes.P.device_dpi
  method width = self#notify ViewportAttributes.P.width
  method height = self#notify ViewportAttributes.P.height
  method initial_scale_factor =
    self#notify ViewportAttributes.P.initial_scale_factor
  method minimum_scale_factor =
    self#notify ViewportAttributes.P.minimum_scale_factor
  method maximum_scale_factor =
    self#notify ViewportAttributes.P.maximum_scale_factor
  method device_pixel_ratio =
    self#notify ViewportAttributes.P.device_pixel_ratio
  method user_scalable = self#notify ViewportAttributes.P.user_scalable
  method valid = self#notify ViewportAttributes.P.valid
end

