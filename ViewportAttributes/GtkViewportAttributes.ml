open Gobject
open Data
open GtkWebkitTypes
module ViewportAttributes = struct
  include GtkViewportAttributesProps.ViewportAttributes
  
  external recompute : viewportattributes Gtk.obj -> unit = "ml_webkit_viewport_attributes_recompute"
end