open GObj
open Gobject
open Gtk
open GtkViewportAttributes
open OGtkViewportAttributesProps

class viewport_attributes_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit viewport_attributes_props
  method recompute = ViewportAttributes.recompute obj_
end

class viewport_attributes obj_ = object(self)
  method as_viewportattributes : GtkWebkitTypes.viewportattributes obj = obj_
  inherit viewport_attributes_skel obj_
end



let wrap_viewportattributes obj = new viewport_attributes (unsafe_cast obj)
let unwrap_viewportattributes obj = unsafe_cast obj#as_viewportattributes
let conv_viewportattributes =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_viewportattributes c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_viewportattributes c))) }

