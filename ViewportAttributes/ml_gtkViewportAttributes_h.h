#include "ViewportAttributes/ml_gtkViewportAttributes_tags_h.h"

#define WebkitViewportAttributes_val(val) check_cast(WEBKIT_VIEWPORT_ATTRIBUTES,val)
#define Val_webkit_viewport_attributes(val) Val_GtkAny(val)
