open GObj
open Gobject
open Gtk
open GtkWebWindowFeatures
open OGtkWebWindowFeaturesProps

class web_window_features_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_window_features_props
  method equal = WebWindowFeatures.equal obj_
end

class web_window_features obj_ = object(self)
  method as_webwindowfeatures : GtkWebkitTypes.webwindowfeatures obj = obj_
  inherit web_window_features_skel obj_
end

let web_window_features () =
  new web_window_features (WebWindowFeatures.create ())

let wrap_webwindowfeatures obj = new web_window_features (unsafe_cast obj)
let unwrap_webwindowfeatures obj = unsafe_cast obj#as_webwindowfeatures
let conv_webwindowfeatures =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webwindowfeatures c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webwindowfeatures c))) }

