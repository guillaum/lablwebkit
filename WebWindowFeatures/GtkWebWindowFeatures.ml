open Gobject
open Data
open GtkWebkitTypes
module WebWindowFeatures = struct
  include GtkWebWindowFeaturesProps.WebWindowFeatures
  external create : unit -> webwindowfeatures Gtk.obj = "ml_webkit_web_window_features_new"
  external equal : webwindowfeatures Gtk.obj -> webwindowfeatures Gtk.obj -> bool = "ml_webkit_web_window_features_equal"
end