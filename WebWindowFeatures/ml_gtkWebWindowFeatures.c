#include "ml_gtkWebkit.h"
#include "ml_gtkWebWindowFeatures_tags_c.h"



ML_0(webkit_web_window_features_get_type, Val_gtype)

ML_0(webkit_web_window_features_new, Val_webkit_web_window_features)

ML_2(webkit_web_window_features_equal, WebkitWebWindowFeatures_val, WebkitWebWindowFeatures_val, Val_bool)

