#include "WebWindowFeatures/ml_gtkWebWindowFeatures_tags_h.h"

#define WebkitWebWindowFeatures_val(val) check_cast(WEBKIT_WEB_WINDOW_FEATURES,val)
#define Val_webkit_web_window_features(val) Val_GtkAny(val)
