open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebWindowFeaturesProps

class virtual web_window_features_props = object
  val virtual obj : _ obj
  method set_x = set WebWindowFeatures.P.x obj
  method set_y = set WebWindowFeatures.P.y obj
  method set_width = set WebWindowFeatures.P.width obj
  method set_height = set WebWindowFeatures.P.height obj
  method set_toolbar_visible = set WebWindowFeatures.P.toolbar_visible obj
  method set_statusbar_visible =
    set WebWindowFeatures.P.statusbar_visible obj
  method set_scrollbar_visible =
    set WebWindowFeatures.P.scrollbar_visible obj
  method set_menubar_visible = set WebWindowFeatures.P.menubar_visible obj
  method set_locationbar_visible =
    set WebWindowFeatures.P.locationbar_visible obj
  method set_fullscreen = set WebWindowFeatures.P.fullscreen obj
  method x = get WebWindowFeatures.P.x obj
  method y = get WebWindowFeatures.P.y obj
  method width = get WebWindowFeatures.P.width obj
  method height = get WebWindowFeatures.P.height obj
  method toolbar_visible = get WebWindowFeatures.P.toolbar_visible obj
  method statusbar_visible = get WebWindowFeatures.P.statusbar_visible obj
  method scrollbar_visible = get WebWindowFeatures.P.scrollbar_visible obj
  method menubar_visible = get WebWindowFeatures.P.menubar_visible obj
  method locationbar_visible =
    get WebWindowFeatures.P.locationbar_visible obj
  method fullscreen = get WebWindowFeatures.P.fullscreen obj
end

class virtual web_window_features_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method x = self#notify WebWindowFeatures.P.x
  method y = self#notify WebWindowFeatures.P.y
  method width = self#notify WebWindowFeatures.P.width
  method height = self#notify WebWindowFeatures.P.height
  method toolbar_visible = self#notify WebWindowFeatures.P.toolbar_visible
  method statusbar_visible =
    self#notify WebWindowFeatures.P.statusbar_visible
  method scrollbar_visible =
    self#notify WebWindowFeatures.P.scrollbar_visible
  method menubar_visible = self#notify WebWindowFeatures.P.menubar_visible
  method locationbar_visible =
    self#notify WebWindowFeatures.P.locationbar_visible
  method fullscreen = self#notify WebWindowFeatures.P.fullscreen
end

