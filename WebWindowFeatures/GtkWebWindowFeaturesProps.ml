open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebWindowFeatures = struct
  let cast w : webwindowfeatures obj = try_cast w "WebkitWebWindowFeatures"
  module P = struct
    let x : ([>`webwindowfeatures],_) property = {name="x"; conv=int}
    let y : ([>`webwindowfeatures],_) property = {name="y"; conv=int}
    let width : ([>`webwindowfeatures],_) property = {name="width"; conv=int}
    let height : ([>`webwindowfeatures],_) property =
      {name="height"; conv=int}
    let toolbar_visible : ([>`webwindowfeatures],_) property =
      {name="toolbar-visible"; conv=boolean}
    let statusbar_visible : ([>`webwindowfeatures],_) property =
      {name="statusbar-visible"; conv=boolean}
    let scrollbar_visible : ([>`webwindowfeatures],_) property =
      {name="scrollbar-visible"; conv=boolean}
    let menubar_visible : ([>`webwindowfeatures],_) property =
      {name="menubar-visible"; conv=boolean}
    let locationbar_visible : ([>`webwindowfeatures],_) property =
      {name="locationbar-visible"; conv=boolean}
    let fullscreen : ([>`webwindowfeatures],_) property =
      {name="fullscreen"; conv=boolean}
  end
  let create pl : webwindowfeatures obj =
    Gobject.unsafe_create "WebkitWebWindowFeatures" pl
  let make_params ~cont pl ?x ?y ?width ?height ?toolbar_visible
      ?statusbar_visible ?scrollbar_visible ?menubar_visible
      ?locationbar_visible ?fullscreen =
    let pl = (
      may_cons P.x x (
      may_cons P.y y (
      may_cons P.width width (
      may_cons P.height height (
      may_cons P.toolbar_visible toolbar_visible (
      may_cons P.statusbar_visible statusbar_visible (
      may_cons P.scrollbar_visible scrollbar_visible (
      may_cons P.menubar_visible menubar_visible (
      may_cons P.locationbar_visible locationbar_visible (
      may_cons P.fullscreen fullscreen pl)))))))))) in
    cont pl
end

