#define __cdecl
#define __MINGW_ATTRIB_PURE
#define _CRTIMP
#define __MINGW_ATTRIB_DEPRECATED_MSVC2005
#define __CRT_INLINE
#define __CRT_NO_INLINE
#define __MINGW_EXTENSION

#define __declspec(e) e
#define CONST const

#define G_BEGIN_DECLS
#define G_END_DECLS
#define WEBKIT_API
#define G_CONST_RETURN const

#define G_GNUC_CONST
#define G_GNUC_PURE
/* #defines to help yacfe's parsing */
#define G_BEGIN_DECLS
#define G_END_DECLS
#define WEBKIT_API
#define G_CONST_RETURN const
