open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module SecurityOrigin = struct
  let cast w : securityorigin obj = try_cast w "WebkitSecurityOrigin"
  module P = struct
    let protocol : ([>`securityorigin],_) property =
      {name="protocol"; conv=string}
    let host : ([>`securityorigin],_) property = {name="host"; conv=string}
    let port : ([>`securityorigin],_) property = {name="port"; conv=uint}
    let web_database_usage : ([>`securityorigin],_) property =
      {name="web-database-usage"; conv=uint64}
    let web_database_quota : ([>`securityorigin],_) property =
      {name="web-database-quota"; conv=uint64}
  end
  let create pl : securityorigin obj =
    Gobject.unsafe_create "WebkitSecurityOrigin" pl
  let make_params ~cont pl ?web_database_quota =
    let pl = (may_cons P.web_database_quota web_database_quota pl) in
    cont pl
end

