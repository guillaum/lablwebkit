#include "ml_gtkWebkit.h"
#include "ml_gtkSecurityOrigin_tags_c.h"



ML_0(webkit_security_origin_get_type, Val_gtype)

ML_1(webkit_security_origin_get_protocol, WebkitSecurityOrigin_val, Val_string)

ML_1(webkit_security_origin_get_host, WebkitSecurityOrigin_val, Val_string)

ML_1(webkit_security_origin_get_port, WebkitSecurityOrigin_val, Val_uint)

ML_1(webkit_security_origin_get_web_database_usage, WebkitSecurityOrigin_val, Val_uint64_t)

ML_1(webkit_security_origin_get_web_database_quota, WebkitSecurityOrigin_val, Val_uint64_t)

ML_2(webkit_security_origin_set_web_database_quota, WebkitSecurityOrigin_val, Uint64_t_val, Unit)

