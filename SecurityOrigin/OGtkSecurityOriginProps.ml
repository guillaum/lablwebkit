open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkSecurityOriginProps

class virtual security_origin_props = object
  val virtual obj : _ obj
  method set_web_database_quota = set SecurityOrigin.P.web_database_quota obj
  method protocol = get SecurityOrigin.P.protocol obj
  method host = get SecurityOrigin.P.host obj
  method port = get SecurityOrigin.P.port obj
  method web_database_usage = get SecurityOrigin.P.web_database_usage obj
  method web_database_quota = get SecurityOrigin.P.web_database_quota obj
end

class virtual security_origin_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method protocol = self#notify SecurityOrigin.P.protocol
  method host = self#notify SecurityOrigin.P.host
  method port = self#notify SecurityOrigin.P.port
  method web_database_usage = self#notify SecurityOrigin.P.web_database_usage
  method web_database_quota = self#notify SecurityOrigin.P.web_database_quota
end

