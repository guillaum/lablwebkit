open GObj
open Gobject
open Gtk
open GtkSecurityOrigin
open OGtkSecurityOriginProps

class security_origin_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit security_origin_props
end

class security_origin obj_ = object(self)
  method as_securityorigin : GtkWebkitTypes.securityorigin obj = obj_
  inherit security_origin_skel obj_
end



let wrap_securityorigin obj = new security_origin (unsafe_cast obj)
let unwrap_securityorigin obj = unsafe_cast obj#as_securityorigin
let conv_securityorigin =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_securityorigin c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_securityorigin c))) }

