open GObj
open Gobject
open Gtk
open GtkFaviconDatabase
open OGtkFaviconDatabaseProps

class favicon_database_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit favicon_database_props
  method get_favicon_uri = FaviconDatabase.get_favicon_uri obj_
  method clear = FaviconDatabase.clear obj_
end

class favicon_database_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit favicon_database_sigs
end

class favicon_database obj_ = object(self)
  method as_favicondatabase : GtkWebkitTypes.favicondatabase obj = obj_
  inherit favicon_database_skel obj_
  method connect = new favicon_database_signals obj_
end



let wrap_favicondatabase obj = new favicon_database (unsafe_cast obj)
let unwrap_favicondatabase obj = unsafe_cast obj#as_favicondatabase
let conv_favicondatabase =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_favicondatabase c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_favicondatabase c))) }

