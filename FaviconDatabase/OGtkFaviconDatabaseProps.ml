open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkFaviconDatabaseProps

class virtual favicon_database_props = object
  val virtual obj : _ obj
  method set_path = set FaviconDatabase.P.path obj
  method path = get FaviconDatabase.P.path obj
end

class virtual favicon_database_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method path = self#notify FaviconDatabase.P.path
end

class virtual favicon_database_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method icon_loaded = self#connect FaviconDatabase.S.icon_loaded
  method notify_path ~callback = self#notify FaviconDatabase.P.path ~callback
end

