open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module FaviconDatabase = struct
  let cast w : favicondatabase obj = try_cast w "WebkitFaviconDatabase"
  module P = struct
    let path : ([>`favicondatabase],_) property = {name="path"; conv=string}
  end
  module S = struct
    open GtkSignal
    let icon_loaded =
      {name="icon_loaded"; classe=`favicondatabase; marshaller=fun f ->
       marshal1 string "WebkitFaviconDatabase::icon_loaded" f}
  end
  let create pl : favicondatabase obj =
    Gobject.unsafe_create "WebkitFaviconDatabase" pl
  let make_params ~cont pl ?path =
    let pl = (may_cons P.path path pl) in
    cont pl
end

