#include "ml_gtkWebkit.h"
#include "ml_gtkFaviconDatabase_tags_c.h"



ML_0(webkit_favicon_database_get_type, Val_gtype)

ML_1(webkit_favicon_database_get_path, WebkitFaviconDatabase_val, Val_string)

ML_2(webkit_favicon_database_set_path, WebkitFaviconDatabase_val, String_val, Unit)

ML_2(webkit_favicon_database_get_favicon_uri, WebkitFaviconDatabase_val, String_val, Val_string)

ML_1(webkit_favicon_database_clear, WebkitFaviconDatabase_val, Unit)

