open Gobject
open Data
open GtkWebkitTypes
module FaviconDatabase = struct
  include GtkFaviconDatabaseProps.FaviconDatabase
  
  external get_favicon_uri : favicondatabase Gtk.obj -> string -> string = "ml_webkit_favicon_database_get_favicon_uri"
  external clear : favicondatabase Gtk.obj -> unit = "ml_webkit_favicon_database_clear"
end