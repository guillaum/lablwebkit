#include "ml_gtkWebkit.h"
#include "ml_gtkWebView_tags_c.h"

#define WebkitNavigationResponse_val Webkit_navigation_response_val
#define WebkitWebViewTargetInfo_val Webkit_web_view_target_info_val
#define WebkitWebViewViewMode_val Webkit_web_view_view_mode_val
#define WebkitSelectionAffinity_val Webkit_selection_affinity_val
#define WebkitInsertAction_val Webkit_insert_action_val

ML_0(webkit_web_view_get_type, Val_gtype)

ML_0(webkit_web_view_new, Val_gtk_widget)

ML_1(webkit_web_view_get_title, WebkitWebView_val, Val_string)

ML_1(webkit_web_view_get_uri, WebkitWebView_val, Val_string)

ML_2(webkit_web_view_set_maintains_back_forward_list, WebkitWebView_val, Bool_val, Unit)

ML_1(webkit_web_view_get_back_forward_list, WebkitWebView_val, Val_webkit_web_back_forward_list)

ML_2(webkit_web_view_go_to_back_forward_item, WebkitWebView_val, WebkitWebHistoryItem_val, Val_bool)

ML_1(webkit_web_view_can_go_back, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_can_go_back_or_forward, WebkitWebView_val, Int_val, Val_bool)

ML_1(webkit_web_view_can_go_forward, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_go_back, WebkitWebView_val, Unit)

ML_2(webkit_web_view_go_back_or_forward, WebkitWebView_val, Int_val, Unit)

ML_1(webkit_web_view_go_forward, WebkitWebView_val, Unit)

ML_1(webkit_web_view_stop_loading, WebkitWebView_val, Unit)

ML_2(webkit_web_view_open, WebkitWebView_val, String_val, Unit)

ML_1(webkit_web_view_reload, WebkitWebView_val, Unit)

ML_1(webkit_web_view_reload_bypass_cache, WebkitWebView_val, Unit)

ML_2(webkit_web_view_load_uri, WebkitWebView_val, String_val, Unit)

ML_5(webkit_web_view_load_string, WebkitWebView_val, String_val, String_val, String_val, String_val, Unit)

ML_3(webkit_web_view_load_html_string, WebkitWebView_val, String_val, String_val, Unit)

ML_2(webkit_web_view_load_request, WebkitWebView_val, WebkitNetworkRequest_val, Unit)

ML_5(webkit_web_view_search_text, WebkitWebView_val, String_val, Bool_val, Bool_val, Bool_val, Val_bool)

ML_4(webkit_web_view_mark_text_matches, WebkitWebView_val, String_val, Bool_val, Uint_val, Val_uint)

ML_2(webkit_web_view_set_highlight_text_matches, WebkitWebView_val, Bool_val, Unit)

ML_1(webkit_web_view_unmark_text_matches, WebkitWebView_val, Unit)

ML_1(webkit_web_view_get_main_frame, WebkitWebView_val, Val_webkit_web_frame)

ML_1(webkit_web_view_get_focused_frame, WebkitWebView_val, Val_webkit_web_frame)

ML_2(webkit_web_view_execute_script, WebkitWebView_val, String_val, Unit)

ML_1(webkit_web_view_can_cut_clipboard, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_can_copy_clipboard, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_can_paste_clipboard, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_cut_clipboard, WebkitWebView_val, Unit)

ML_1(webkit_web_view_copy_clipboard, WebkitWebView_val, Unit)

ML_1(webkit_web_view_paste_clipboard, WebkitWebView_val, Unit)

ML_1(webkit_web_view_delete_selection, WebkitWebView_val, Unit)

ML_1(webkit_web_view_has_selection, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_select_all, WebkitWebView_val, Unit)

ML_1(webkit_web_view_get_editable, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_set_editable, WebkitWebView_val, Bool_val, Unit)

ML_2(webkit_web_view_set_settings, WebkitWebView_val, WebkitWebSettings_val, Unit)

ML_1(webkit_web_view_get_settings, WebkitWebView_val, Val_webkit_web_settings)

ML_1(webkit_web_view_get_inspector, WebkitWebView_val, Val_webkit_web_inspector)

ML_1(webkit_web_view_get_window_features, WebkitWebView_val, Val_webkit_web_window_features)

ML_2(webkit_web_view_can_show_mime_type, WebkitWebView_val, String_val, Val_bool)

ML_1(webkit_web_view_get_transparent, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_set_transparent, WebkitWebView_val, Bool_val, Unit)

ML_1(webkit_web_view_get_zoom_level, WebkitWebView_val, Val_float)

ML_2(webkit_web_view_set_zoom_level, WebkitWebView_val, Float_val, Unit)

ML_1(webkit_web_view_zoom_in, WebkitWebView_val, Unit)

ML_1(webkit_web_view_zoom_out, WebkitWebView_val, Unit)

ML_1(webkit_web_view_get_full_content_zoom, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_set_full_content_zoom, WebkitWebView_val, Bool_val, Unit)

ML_1(webkit_web_view_get_encoding, WebkitWebView_val, Val_string)

ML_2(webkit_web_view_set_custom_encoding, WebkitWebView_val, String_val, Unit)

ML_1(webkit_web_view_get_custom_encoding, WebkitWebView_val, Val_char)

ML_3(webkit_web_view_move_cursor, WebkitWebView_val, GtkMovementStep_val, Int_val, Unit)

ML_1(webkit_web_view_get_load_status, WebkitWebView_val, Val_webkit_load_status)

ML_1(webkit_web_view_get_progress, WebkitWebView_val, Val_double)

ML_1(webkit_web_view_undo, WebkitWebView_val, Unit)

ML_1(webkit_web_view_can_undo, WebkitWebView_val, Val_bool)

ML_1(webkit_web_view_redo, WebkitWebView_val, Unit)

ML_1(webkit_web_view_can_redo, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_set_view_source_mode, WebkitWebView_val, Bool_val, Unit)

ML_1(webkit_web_view_get_view_source_mode, WebkitWebView_val, Val_bool)

ML_2(webkit_web_view_get_hit_test_result, WebkitWebView_val, GdkEventButton_val, Val_webkit_hit_test_result)

ML_1(webkit_web_view_get_icon_uri, WebkitWebView_val, Val_string)

ML_1(webkit_web_view_get_viewport_attributes, WebkitWebView_val, Val_webkit_viewport_attributes)

