/* webkit_navigation_response : conversion table */
const lookup_info ml_table_webkit_navigation_response[] = {
  { 0, 3 },
  { MLTAG_IGNORE, WEBKIT_NAVIGATION_RESPONSE_IGNORE },
  { MLTAG_DOWNLOAD, WEBKIT_NAVIGATION_RESPONSE_DOWNLOAD },
  { MLTAG_ACCEPT, WEBKIT_NAVIGATION_RESPONSE_ACCEPT },
};

/* webkit_web_view_target_info : conversion table */
const lookup_info ml_table_webkit_web_view_target_info[] = {
  { 0, 5 },
  { MLTAG_URI_LIST, WEBKIT_WEB_VIEW_TARGET_INFO_URI_LIST },
  { MLTAG_HTML, WEBKIT_WEB_VIEW_TARGET_INFO_HTML },
  { MLTAG_TEXT, WEBKIT_WEB_VIEW_TARGET_INFO_TEXT },
  { MLTAG_IMAGE, WEBKIT_WEB_VIEW_TARGET_INFO_IMAGE },
  { MLTAG_NETSCAPE_URL, WEBKIT_WEB_VIEW_TARGET_INFO_NETSCAPE_URL },
};

/* webkit_web_view_view_mode : conversion table */
const lookup_info ml_table_webkit_web_view_view_mode[] = {
  { 0, 5 },
  { MLTAG_WINDOWED, WEBKIT_WEB_VIEW_VIEW_MODE_WINDOWED },
  { MLTAG_FULLSCREEN, WEBKIT_WEB_VIEW_VIEW_MODE_FULLSCREEN },
  { MLTAG_MAXIMIZED, WEBKIT_WEB_VIEW_VIEW_MODE_MAXIMIZED },
  { MLTAG_MINIMIZED, WEBKIT_WEB_VIEW_VIEW_MODE_MINIMIZED },
  { MLTAG_FLOATING, WEBKIT_WEB_VIEW_VIEW_MODE_FLOATING },
};

/* webkit_selection_affinity : conversion table */
const lookup_info ml_table_webkit_selection_affinity[] = {
  { 0, 2 },
  { MLTAG_UPSTREAM, WEBKIT_SELECTION_AFFINITY_UPSTREAM },
  { MLTAG_DOWNSTREAM, WEBKIT_SELECTION_AFFINITY_DOWNSTREAM },
};

/* webkit_insert_action : conversion table */
const lookup_info ml_table_webkit_insert_action[] = {
  { 0, 3 },
  { MLTAG_DROPPED, WEBKIT_INSERT_ACTION_DROPPED },
  { MLTAG_TYPED, WEBKIT_INSERT_ACTION_TYPED },
  { MLTAG_PASTED, WEBKIT_INSERT_ACTION_PASTED },
};

CAMLprim value ml_webkit_web_view_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_navigation_response,
    ml_table_webkit_web_view_target_info,
    ml_table_webkit_web_view_view_mode,
    ml_table_webkit_selection_affinity,
    ml_table_webkit_insert_action,
  };
  return (value)ml_lookup_tables;}
