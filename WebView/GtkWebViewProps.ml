open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebView = struct
  let cast w : webview obj = try_cast w "WebkitWebView"
  module P = struct
    let name : ([>`webview],_) property = {name="name"; conv=string}
    let width_request : ([>`webview],_) property =
      {name="width-request"; conv=int}
    let height_request : ([>`webview],_) property =
      {name="height-request"; conv=int}
    let visible : ([>`webview],_) property = {name="visible"; conv=boolean}
    let sensitive : ([>`webview],_) property =
      {name="sensitive"; conv=boolean}
    let app_paintable : ([>`webview],_) property =
      {name="app-paintable"; conv=boolean}
    let can_focus : ([>`webview],_) property =
      {name="can-focus"; conv=boolean}
    let has_focus : ([>`webview],_) property =
      {name="has-focus"; conv=boolean}
    let is_focus : ([>`webview],_) property = {name="is-focus"; conv=boolean}
    let can_default : ([>`webview],_) property =
      {name="can-default"; conv=boolean}
    let has_default : ([>`webview],_) property =
      {name="has-default"; conv=boolean}
    let receives_default : ([>`webview],_) property =
      {name="receives-default"; conv=boolean}
    let composite_child : ([>`webview],_) property =
      {name="composite-child"; conv=boolean}
    let style : ([>`webview],_) property =
      {name="style"; conv=(gobject : Gtk.style data_conv)}
    let events : ([>`webview],_) property =
      {name="events"; conv=GdkEnums.event_mask_conv}
    let extension_events : ([>`webview],_) property =
      {name="extension-events"; conv=GdkEnums.extension_mode_conv}
    let no_show_all : ([>`webview],_) property =
      {name="no-show-all"; conv=boolean}
    let has_tooltip : ([>`webview],_) property =
      {name="has-tooltip"; conv=boolean}
    let tooltip_markup : ([>`webview],_) property =
      {name="tooltip-markup"; conv=string}
    let tooltip_text : ([>`webview],_) property =
      {name="tooltip-text"; conv=string}
    let double_buffered : ([>`webview],_) property =
      {name="double-buffered"; conv=boolean}
    let border_width : ([>`webview],_) property =
      {name="border-width"; conv=uint}
    let resize_mode : ([>`webview],_) property =
      {name="resize-mode"; conv=GtkEnums.resize_mode_conv}
    let child : ([>`webview],_) property =
      {name="child"; conv=(gobject : Gtk.widget obj data_conv)}
    let title : ([>`webview],_) property = {name="title"; conv=string}
    let uri : ([>`webview],_) property = {name="uri"; conv=string}
    let editable : ([>`webview],_) property = {name="editable"; conv=boolean}
    let settings : ([>`webview],_) property =
      {name="settings"; conv=GWebSettings.conv_websettings}
    let web_inspector : ([>`webview],_) property =
      {name="web-inspector"; conv=GWebInspector.conv_webinspector}
    let viewport_attributes : ([>`webview],_) property =
      {name="viewport-attributes";
       conv=GViewportAttributes.conv_viewportattributes}
    let window_features : ([>`webview],_) property =
      {name="window-features";
       conv=GWebWindowFeatures.conv_webwindowfeatures}
    let transparent : ([>`webview],_) property =
      {name="transparent"; conv=boolean}
    let zoom_level : ([>`webview],_) property =
      {name="zoom-level"; conv=float}
    let full_content_zoom : ([>`webview],_) property =
      {name="full-content-zoom"; conv=boolean}
    let load_status : ([>`webview],_) property =
      {name="load-status"; conv=webkit_load_status_conv}
    let progress : ([>`webview],_) property = {name="progress"; conv=double}
    let encoding : ([>`webview],_) property = {name="encoding"; conv=string}
    let custom_encoding : ([>`webview],_) property =
      {name="custom-encoding"; conv=string}
    let icon_uri : ([>`webview],_) property = {name="icon-uri"; conv=string}
    let self_scrolling : ([>`webview],_) property =
      {name="self-scrolling"; conv=boolean}
  end
  module S = struct
    open GtkSignal
    let icon_loaded =
      {name="icon_loaded"; classe=`webview; marshaller=fun f ->
       marshal1 string "WebkitWebView::icon_loaded" f}
    let load_committed =
      {name="load_committed"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe "WebkitWebView::load_committed" f}
    let title_changed =
      {name="title_changed"; classe=`webview; marshaller=fun f ->
       marshal2 GWebFrame.conv_webframe string
         "WebkitWebView::title_changed" f}
    let hovering_over_link =
      {name="hovering_over_link"; classe=`webview; marshaller=fun f ->
       marshal2 string string "WebkitWebView::hovering_over_link" f}
    let resource_request_starting =
      {name="resource_request_starting"; classe=`webview; marshaller=fun f ->
       marshal4 GWebFrame.conv_webframe GWebResource.conv_webresource
         GNetworkRequest.conv_networkrequest
         GNetworkResponse.conv_networkresponse
         "WebkitWebView::resource_request_starting" f}
    let resource_response_received =
      {name="resource_response_received"; classe=`webview;
       marshaller=fun f ->
       marshal3 GWebFrame.conv_webframe GWebResource.conv_webresource
         GNetworkResponse.conv_networkresponse
         "WebkitWebView::resource_response_received" f}
    let resource_load_finished =
      {name="resource_load_finished"; classe=`webview; marshaller=fun f ->
       marshal2 GWebFrame.conv_webframe GWebResource.conv_webresource
         "WebkitWebView::resource_load_finished" f}
    let resource_content_length_received =
      {name="resource_content_length_received"; classe=`webview;
       marshaller=fun f ->
       marshal3 GWebFrame.conv_webframe GWebResource.conv_webresource int
         "WebkitWebView::resource_content_length_received" f}
    let load_finished =
      {name="load_finished"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe "WebkitWebView::load_finished" f}
    let create_web_view =
      {name="create_web_view"; classe=`webview; marshaller=fun f ->
       marshal1_ret ~ret:(gobject : webview obj data_conv)
         GWebFrame.conv_webframe "WebkitWebView::create_web_view" f}
    let web_view_ready =
      {name="web_view_ready"; classe=`webview;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let close_web_view =
      {name="close_web_view"; classe=`webview;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let navigation_requested =
      {name="navigation_requested"; classe=`webview; marshaller=fun f ->
       marshal2_ret ~ret:webkit_navigation_response_conv
         GWebFrame.conv_webframe GNetworkRequest.conv_networkrequest
         "WebkitWebView::navigation_requested" f}
    let new_window_policy_decision_requested =
      {name="new_window_policy_decision_requested"; classe=`webview;
       marshaller=fun f ->
       marshal4_ret ~ret:boolean GWebFrame.conv_webframe
         GNetworkRequest.conv_networkrequest
         GWebNavigationAction.conv_webnavigationaction
         GWebPolicyDecision.conv_webpolicydecision
         "WebkitWebView::new_window_policy_decision_requested" f}
    let navigation_policy_decision_requested =
      {name="navigation_policy_decision_requested"; classe=`webview;
       marshaller=fun f ->
       marshal4_ret ~ret:boolean GWebFrame.conv_webframe
         GNetworkRequest.conv_networkrequest
         GWebNavigationAction.conv_webnavigationaction
         GWebPolicyDecision.conv_webpolicydecision
         "WebkitWebView::navigation_policy_decision_requested" f}
    let mime_type_policy_decision_requested =
      {name="mime_type_policy_decision_requested"; classe=`webview;
       marshaller=fun f ->
       marshal4_ret ~ret:boolean GWebFrame.conv_webframe
         GNetworkRequest.conv_networkrequest string
         GWebPolicyDecision.conv_webpolicydecision
         "WebkitWebView::mime_type_policy_decision_requested" f}
    let download_requested =
      {name="download_requested"; classe=`webview; marshaller=fun f ->
       marshal1_ret ~ret:boolean (gobject : unit obj data_conv)
         "WebkitWebView::download_requested" f}
    let load_started =
      {name="load_started"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe "WebkitWebView::load_started" f}
    let load_progress_changed =
      {name="load_progress_changed"; classe=`webview; marshaller=fun f ->
       marshal1 int "WebkitWebView::load_progress_changed" f}
    let onload_event =
      {name="onload_event"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe "WebkitWebView::onload_event" f}
    let print_requested =
      {name="print_requested"; classe=`webview; marshaller=fun f ->
       marshal1_ret ~ret:boolean GWebFrame.conv_webframe
         "WebkitWebView::print_requested" f}
    let status_bar_text_changed =
      {name="status_bar_text_changed"; classe=`webview; marshaller=fun f ->
       marshal1 string "WebkitWebView::status_bar_text_changed" f}
    let console_message =
      {name="console_message"; classe=`webview; marshaller=fun f ->
       marshal3_ret ~ret:boolean string int string
         "WebkitWebView::console_message" f}
    let script_alert =
      {name="script_alert"; classe=`webview; marshaller=fun f ->
       marshal2_ret ~ret:boolean GWebFrame.conv_webframe string
         "WebkitWebView::script_alert" f}
    let select_all =
      {name="select_all"; classe=`webview; marshaller=marshal_unit}
    let cut_clipboard =
      {name="cut_clipboard"; classe=`webview; marshaller=marshal_unit}
    let copy_clipboard =
      {name="copy_clipboard"; classe=`webview; marshaller=marshal_unit}
    let paste_clipboard =
      {name="paste_clipboard"; classe=`webview; marshaller=marshal_unit}
    let undo = {name="undo"; classe=`webview; marshaller=marshal_unit}
    let redo = {name="redo"; classe=`webview; marshaller=marshal_unit}
    let move_cursor =
      {name="move_cursor"; classe=`webview; marshaller=fun f ->
       marshal2_ret ~ret:boolean GtkEnums.movement_step_conv int
         "WebkitWebView::move_cursor" f}
    let database_quota_exceeded =
      {name="database_quota_exceeded"; classe=`webview; marshaller=fun f ->
       marshal2 (gobject : unit obj data_conv) (gobject : unit obj data_conv)
         "WebkitWebView::database_quota_exceeded" f}
    let geolocation_policy_decision_requested =
      {name="geolocation_policy_decision_requested"; classe=`webview;
       marshaller=fun f ->
       marshal2_ret ~ret:boolean GWebFrame.conv_webframe
         GGeolocationPolicyDecision.conv_geolocationpolicydecision
         "WebkitWebView::geolocation_policy_decision_requested" f}
    let geolocation_policy_decision_cancelled =
      {name="geolocation_policy_decision_cancelled"; classe=`webview;
       marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe
         "WebkitWebView::geolocation_policy_decision_cancelled" f}
    let document_load_finished =
      {name="document_load_finished"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe
         "WebkitWebView::document_load_finished" f}
    let frame_created =
      {name="frame_created"; classe=`webview; marshaller=fun f ->
       marshal1 GWebFrame.conv_webframe "WebkitWebView::frame_created" f}
    let editing_began =
      {name="editing_began"; classe=`webview; marshaller=marshal_unit}
    let user_changed_contents =
      {name="user_changed_contents"; classe=`webview;
       marshaller=marshal_unit}
    let editing_ended =
      {name="editing_ended"; classe=`webview; marshaller=marshal_unit}
    let selection_changed =
      {name="selection_changed"; classe=`webview; marshaller=marshal_unit}
    let viewport_attributes_recompute_requested =
      {name="viewport_attributes_recompute_requested"; classe=`webview;
       marshaller=fun f ->
       marshal1 GViewportAttributes.conv_viewportattributes
         "WebkitWebView::viewport_attributes_recompute_requested" f}
    let viewport_attributes_changed =
      {name="viewport_attributes_changed"; classe=`webview;
       marshaller=fun f ->
       marshal1 GViewportAttributes.conv_viewportattributes
         "WebkitWebView::viewport_attributes_changed" f}
  end
  let create ?self_scrolling pl : webview obj =
    let pl = (may_cons P.self_scrolling self_scrolling pl) in
    GtkObject.make "WebkitWebView" pl
  let make_params ~cont pl ?name ?width_request ?height_request ?visible
      ?sensitive ?app_paintable ?can_focus ?has_focus ?is_focus ?can_default
      ?has_default ?receives_default ?style ?events ?extension_events
      ?no_show_all ?has_tooltip ?tooltip_markup ?tooltip_text
      ?double_buffered ?border_width ?resize_mode ?child ?editable ?settings
      ?window_features ?transparent ?zoom_level ?full_content_zoom
      ?custom_encoding =
    let pl = (
      may_cons P.name name (
      may_cons P.width_request width_request (
      may_cons P.height_request height_request (
      may_cons P.visible visible (
      may_cons P.sensitive sensitive (
      may_cons P.app_paintable app_paintable (
      may_cons P.can_focus can_focus (
      may_cons P.has_focus has_focus (
      may_cons P.is_focus is_focus (
      may_cons P.can_default can_default (
      may_cons P.has_default has_default (
      may_cons P.receives_default receives_default (
      may_cons P.style style (
      may_cons P.events events (
      may_cons P.extension_events extension_events (
      may_cons P.no_show_all no_show_all (
      may_cons P.has_tooltip has_tooltip (
      may_cons P.tooltip_markup tooltip_markup (
      may_cons P.tooltip_text tooltip_text (
      may_cons P.double_buffered double_buffered (
      may_cons P.border_width border_width (
      may_cons P.resize_mode resize_mode (
      may_cons P.child child (
      may_cons P.editable editable (
      may_cons P.settings settings (
      may_cons P.window_features window_features (
      may_cons P.transparent transparent (
      may_cons P.zoom_level zoom_level (
      may_cons P.full_content_zoom full_content_zoom (
      may_cons P.custom_encoding custom_encoding pl)))))))))))))))))))))))))))))) in
    cont pl
end

