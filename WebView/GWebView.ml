open GObj
open Gobject
open Gtk
open GtkWebView
open OGtkWebViewProps

class web_view_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_view_props
  inherit GContainer.container obj_
  method set_maintains_back_forward_list = WebView.set_maintains_back_forward_list obj_
  method get_back_forward_list = WebView.get_back_forward_list obj_
  method go_to_back_forward_item = WebView.go_to_back_forward_item obj_
  method can_go_back = WebView.can_go_back obj_
  method can_go_back_or_forward = WebView.can_go_back_or_forward obj_
  method can_go_forward = WebView.can_go_forward obj_
  method go_back = WebView.go_back obj_
  method go_back_or_forward = WebView.go_back_or_forward obj_
  method go_forward = WebView.go_forward obj_
  method stop_loading = WebView.stop_loading obj_
  method reload = WebView.reload obj_
  method reload_bypass_cache = WebView.reload_bypass_cache obj_
  method load_uri = WebView.load_uri obj_
  method load_string = WebView.load_string obj_
  method load_html_string = WebView.load_html_string obj_
  method load_request = WebView.load_request obj_
  method search_text = WebView.search_text obj_
  method mark_text_matches = WebView.mark_text_matches obj_
  method set_highlight_text_matches = WebView.set_highlight_text_matches obj_
  method unmark_text_matches = WebView.unmark_text_matches obj_
  method get_main_frame = WebView.get_main_frame obj_
  method get_focused_frame = WebView.get_focused_frame obj_
  method execute_script = WebView.execute_script obj_
  method can_cut_clipboard = WebView.can_cut_clipboard obj_
  method can_copy_clipboard = WebView.can_copy_clipboard obj_
  method can_paste_clipboard = WebView.can_paste_clipboard obj_
  method cut_clipboard = WebView.cut_clipboard obj_
  method copy_clipboard = WebView.copy_clipboard obj_
  method paste_clipboard = WebView.paste_clipboard obj_
  method delete_selection = WebView.delete_selection obj_
  method has_selection = WebView.has_selection obj_
  method select_all = WebView.select_all obj_
  method get_inspector = WebView.get_inspector obj_
  method can_show_mime_type = WebView.can_show_mime_type obj_
  method zoom_in = WebView.zoom_in obj_
  method zoom_out = WebView.zoom_out obj_
  method move_cursor = WebView.move_cursor obj_
  method undo = WebView.undo obj_
  method can_undo = WebView.can_undo obj_
  method redo = WebView.redo obj_
  method can_redo = WebView.can_redo obj_
  method set_view_source_mode = WebView.set_view_source_mode obj_
  method get_view_source_mode = WebView.get_view_source_mode obj_
  method get_hit_test_result = WebView.get_hit_test_result obj_
end

class web_view_signals obj_ = object(self)
  inherit GContainer.container_signals_impl obj_
  inherit web_view_sigs
end

class web_view obj_ = object(self)
  method as_webview : GtkWebkitTypes.webview obj = obj_
  inherit web_view_skel obj_
  method connect = new web_view_signals obj_
end

let web_view ?packing ?show () =
  let o = new web_view (WebView.create ()) in
  pack_return o ~packing ~show

let wrap_webview obj = new web_view (unsafe_cast obj)
let unwrap_webview obj = unsafe_cast obj#as_webview
let conv_webview =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webview c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webview c))) }

