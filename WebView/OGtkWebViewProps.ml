open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebViewProps

class virtual web_view_props = object
  val virtual obj : _ obj
  method set_name = set WebView.P.name obj
  method set_width_request = set WebView.P.width_request obj
  method set_height_request = set WebView.P.height_request obj
  method set_visible = set WebView.P.visible obj
  method set_sensitive = set WebView.P.sensitive obj
  method set_app_paintable = set WebView.P.app_paintable obj
  method set_can_focus = set WebView.P.can_focus obj
  method set_has_focus = set WebView.P.has_focus obj
  method set_is_focus = set WebView.P.is_focus obj
  method set_can_default = set WebView.P.can_default obj
  method set_has_default = set WebView.P.has_default obj
  method set_receives_default = set WebView.P.receives_default obj
  method set_style = set WebView.P.style obj
  method set_events = set WebView.P.events obj
  method set_extension_events = set WebView.P.extension_events obj
  method set_no_show_all = set WebView.P.no_show_all obj
  method set_has_tooltip = set WebView.P.has_tooltip obj
  method set_tooltip_markup = set WebView.P.tooltip_markup obj
  method set_tooltip_text = set WebView.P.tooltip_text obj
  method set_double_buffered = set WebView.P.double_buffered obj
  method set_border_width = set WebView.P.border_width obj
  method set_resize_mode = set WebView.P.resize_mode obj
  method set_child = set {WebView.P.child with conv=GObj.conv_widget} obj
  method set_editable = set WebView.P.editable obj
  method set_settings = set WebView.P.settings obj
  method set_window_features = set WebView.P.window_features obj
  method set_transparent = set WebView.P.transparent obj
  method set_zoom_level = set WebView.P.zoom_level obj
  method set_full_content_zoom = set WebView.P.full_content_zoom obj
  method set_custom_encoding = set WebView.P.custom_encoding obj
  method name = get WebView.P.name obj
  method width_request = get WebView.P.width_request obj
  method height_request = get WebView.P.height_request obj
  method visible = get WebView.P.visible obj
  method sensitive = get WebView.P.sensitive obj
  method app_paintable = get WebView.P.app_paintable obj
  method can_focus = get WebView.P.can_focus obj
  method has_focus = get WebView.P.has_focus obj
  method is_focus = get WebView.P.is_focus obj
  method can_default = get WebView.P.can_default obj
  method has_default = get WebView.P.has_default obj
  method receives_default = get WebView.P.receives_default obj
  method composite_child = get WebView.P.composite_child obj
  method style = get WebView.P.style obj
  method events = get WebView.P.events obj
  method extension_events = get WebView.P.extension_events obj
  method no_show_all = get WebView.P.no_show_all obj
  method has_tooltip = get WebView.P.has_tooltip obj
  method tooltip_markup = get WebView.P.tooltip_markup obj
  method tooltip_text = get WebView.P.tooltip_text obj
  method double_buffered = get WebView.P.double_buffered obj
  method border_width = get WebView.P.border_width obj
  method resize_mode = get WebView.P.resize_mode obj
  method title = get WebView.P.title obj
  method uri = get WebView.P.uri obj
  method editable = get WebView.P.editable obj
  method settings = get WebView.P.settings obj
  method web_inspector = get WebView.P.web_inspector obj
  method viewport_attributes = get WebView.P.viewport_attributes obj
  method window_features = get WebView.P.window_features obj
  method transparent = get WebView.P.transparent obj
  method zoom_level = get WebView.P.zoom_level obj
  method full_content_zoom = get WebView.P.full_content_zoom obj
  method load_status = get WebView.P.load_status obj
  method progress = get WebView.P.progress obj
  method encoding = get WebView.P.encoding obj
  method custom_encoding = get WebView.P.custom_encoding obj
  method icon_uri = get WebView.P.icon_uri obj
  method self_scrolling = get WebView.P.self_scrolling obj
end

class virtual web_view_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method name = self#notify WebView.P.name
  method width_request = self#notify WebView.P.width_request
  method height_request = self#notify WebView.P.height_request
  method visible = self#notify WebView.P.visible
  method sensitive = self#notify WebView.P.sensitive
  method app_paintable = self#notify WebView.P.app_paintable
  method can_focus = self#notify WebView.P.can_focus
  method has_focus = self#notify WebView.P.has_focus
  method is_focus = self#notify WebView.P.is_focus
  method can_default = self#notify WebView.P.can_default
  method has_default = self#notify WebView.P.has_default
  method receives_default = self#notify WebView.P.receives_default
  method composite_child = self#notify WebView.P.composite_child
  method style = self#notify WebView.P.style
  method events = self#notify WebView.P.events
  method extension_events = self#notify WebView.P.extension_events
  method no_show_all = self#notify WebView.P.no_show_all
  method has_tooltip = self#notify WebView.P.has_tooltip
  method tooltip_markup = self#notify WebView.P.tooltip_markup
  method tooltip_text = self#notify WebView.P.tooltip_text
  method double_buffered = self#notify WebView.P.double_buffered
  method border_width = self#notify WebView.P.border_width
  method resize_mode = self#notify WebView.P.resize_mode
  method title = self#notify WebView.P.title
  method uri = self#notify WebView.P.uri
  method editable = self#notify WebView.P.editable
  method settings = self#notify WebView.P.settings
  method web_inspector = self#notify WebView.P.web_inspector
  method viewport_attributes = self#notify WebView.P.viewport_attributes
  method window_features = self#notify WebView.P.window_features
  method transparent = self#notify WebView.P.transparent
  method zoom_level = self#notify WebView.P.zoom_level
  method full_content_zoom = self#notify WebView.P.full_content_zoom
  method load_status = self#notify WebView.P.load_status
  method progress = self#notify WebView.P.progress
  method encoding = self#notify WebView.P.encoding
  method custom_encoding = self#notify WebView.P.custom_encoding
  method icon_uri = self#notify WebView.P.icon_uri
  method self_scrolling = self#notify WebView.P.self_scrolling
end

class virtual web_view_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method icon_loaded = self#connect WebView.S.icon_loaded
  method load_committed = self#connect WebView.S.load_committed
  method title_changed = self#connect WebView.S.title_changed
  method hovering_over_link = self#connect WebView.S.hovering_over_link
  method resource_request_starting =
    self#connect WebView.S.resource_request_starting
  method resource_response_received =
    self#connect WebView.S.resource_response_received
  method resource_load_finished =
    self#connect WebView.S.resource_load_finished
  method resource_content_length_received =
    self#connect WebView.S.resource_content_length_received
  method load_finished = self#connect WebView.S.load_finished
  method create_web_view = self#connect WebView.S.create_web_view
  method web_view_ready = self#connect WebView.S.web_view_ready
  method close_web_view = self#connect WebView.S.close_web_view
  method navigation_requested = self#connect WebView.S.navigation_requested
  method new_window_policy_decision_requested =
    self#connect WebView.S.new_window_policy_decision_requested
  method navigation_policy_decision_requested =
    self#connect WebView.S.navigation_policy_decision_requested
  method mime_type_policy_decision_requested =
    self#connect WebView.S.mime_type_policy_decision_requested
  method download_requested = self#connect WebView.S.download_requested
  method load_started = self#connect WebView.S.load_started
  method load_progress_changed = self#connect WebView.S.load_progress_changed
  method onload_event = self#connect WebView.S.onload_event
  method print_requested = self#connect WebView.S.print_requested
  method status_bar_text_changed =
    self#connect WebView.S.status_bar_text_changed
  method console_message = self#connect WebView.S.console_message
  method script_alert = self#connect WebView.S.script_alert
  method select_all = self#connect WebView.S.select_all
  method cut_clipboard = self#connect WebView.S.cut_clipboard
  method copy_clipboard = self#connect WebView.S.copy_clipboard
  method paste_clipboard = self#connect WebView.S.paste_clipboard
  method undo = self#connect WebView.S.undo
  method redo = self#connect WebView.S.redo
  method move_cursor = self#connect WebView.S.move_cursor
  method database_quota_exceeded =
    self#connect WebView.S.database_quota_exceeded
  method geolocation_policy_decision_requested =
    self#connect WebView.S.geolocation_policy_decision_requested
  method geolocation_policy_decision_cancelled =
    self#connect WebView.S.geolocation_policy_decision_cancelled
  method document_load_finished =
    self#connect WebView.S.document_load_finished
  method frame_created = self#connect WebView.S.frame_created
  method editing_began = self#connect WebView.S.editing_began
  method user_changed_contents = self#connect WebView.S.user_changed_contents
  method editing_ended = self#connect WebView.S.editing_ended
  method selection_changed = self#connect WebView.S.selection_changed
  method viewport_attributes_recompute_requested =
    self#connect WebView.S.viewport_attributes_recompute_requested
  method viewport_attributes_changed =
    self#connect WebView.S.viewport_attributes_changed
  method notify_name ~callback = self#notify WebView.P.name ~callback
  method notify_width_request ~callback =
    self#notify WebView.P.width_request ~callback
  method notify_height_request ~callback =
    self#notify WebView.P.height_request ~callback
  method notify_visible ~callback = self#notify WebView.P.visible ~callback
  method notify_sensitive ~callback =
    self#notify WebView.P.sensitive ~callback
  method notify_app_paintable ~callback =
    self#notify WebView.P.app_paintable ~callback
  method notify_can_focus ~callback =
    self#notify WebView.P.can_focus ~callback
  method notify_has_focus ~callback =
    self#notify WebView.P.has_focus ~callback
  method notify_is_focus ~callback = self#notify WebView.P.is_focus ~callback
  method notify_can_default ~callback =
    self#notify WebView.P.can_default ~callback
  method notify_has_default ~callback =
    self#notify WebView.P.has_default ~callback
  method notify_receives_default ~callback =
    self#notify WebView.P.receives_default ~callback
  method notify_composite_child ~callback =
    self#notify WebView.P.composite_child ~callback
  method notify_style ~callback = self#notify WebView.P.style ~callback
  method notify_events ~callback = self#notify WebView.P.events ~callback
  method notify_extension_events ~callback =
    self#notify WebView.P.extension_events ~callback
  method notify_no_show_all ~callback =
    self#notify WebView.P.no_show_all ~callback
  method notify_has_tooltip ~callback =
    self#notify WebView.P.has_tooltip ~callback
  method notify_tooltip_markup ~callback =
    self#notify WebView.P.tooltip_markup ~callback
  method notify_tooltip_text ~callback =
    self#notify WebView.P.tooltip_text ~callback
  method notify_double_buffered ~callback =
    self#notify WebView.P.double_buffered ~callback
  method notify_border_width ~callback =
    self#notify WebView.P.border_width ~callback
  method notify_resize_mode ~callback =
    self#notify WebView.P.resize_mode ~callback
  method notify_title ~callback = self#notify WebView.P.title ~callback
  method notify_uri ~callback = self#notify WebView.P.uri ~callback
  method notify_editable ~callback = self#notify WebView.P.editable ~callback
  method notify_settings ~callback = self#notify WebView.P.settings ~callback
  method notify_web_inspector ~callback =
    self#notify WebView.P.web_inspector ~callback
  method notify_viewport_attributes ~callback =
    self#notify WebView.P.viewport_attributes ~callback
  method notify_window_features ~callback =
    self#notify WebView.P.window_features ~callback
  method notify_transparent ~callback =
    self#notify WebView.P.transparent ~callback
  method notify_zoom_level ~callback =
    self#notify WebView.P.zoom_level ~callback
  method notify_full_content_zoom ~callback =
    self#notify WebView.P.full_content_zoom ~callback
  method notify_load_status ~callback =
    self#notify WebView.P.load_status ~callback
  method notify_progress ~callback = self#notify WebView.P.progress ~callback
  method notify_encoding ~callback = self#notify WebView.P.encoding ~callback
  method notify_custom_encoding ~callback =
    self#notify WebView.P.custom_encoding ~callback
  method notify_icon_uri ~callback = self#notify WebView.P.icon_uri ~callback
  method notify_self_scrolling ~callback =
    self#notify WebView.P.self_scrolling ~callback
end

