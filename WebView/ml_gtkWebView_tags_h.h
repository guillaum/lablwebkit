/* webkit_navigation_response : tags and macros */
#define MLTAG_DOWNLOAD	((value)(-347341944*2+1))
#define MLTAG_IGNORE	((value)(-984914670*2+1))
#define MLTAG_ACCEPT	((value)(1032404744*2+1))

extern const lookup_info ml_table_webkit_navigation_response[];
#define Val_webkit_navigation_response(data) ml_lookup_from_c (ml_table_webkit_navigation_response, data)
#define Webkit_navigation_response_val(key) ml_lookup_to_c (ml_table_webkit_navigation_response, key)

/* webkit_web_view_target_info : tags and macros */
#define MLTAG_NETSCAPE_URL	((value)(1046521177*2+1))
#define MLTAG_URI_LIST	((value)(-449690255*2+1))
#define MLTAG_IMAGE	((value)(995579707*2+1))
#define MLTAG_TEXT	((value)(934974637*2+1))
#define MLTAG_HTML	((value)(802643307*2+1))

extern const lookup_info ml_table_webkit_web_view_target_info[];
#define Val_webkit_web_view_target_info(data) ml_lookup_from_c (ml_table_webkit_web_view_target_info, data)
#define Webkit_web_view_target_info_val(key) ml_lookup_to_c (ml_table_webkit_web_view_target_info, key)

/* webkit_web_view_view_mode : tags and macros */
#define MLTAG_MINIMIZED	((value)(603351910*2+1))
#define MLTAG_MAXIMIZED	((value)(-96895496*2+1))
#define MLTAG_FULLSCREEN	((value)(-339890629*2+1))
#define MLTAG_FLOATING	((value)(925188294*2+1))
#define MLTAG_WINDOWED	((value)(-682403761*2+1))

extern const lookup_info ml_table_webkit_web_view_view_mode[];
#define Val_webkit_web_view_view_mode(data) ml_lookup_from_c (ml_table_webkit_web_view_view_mode, data)
#define Webkit_web_view_view_mode_val(key) ml_lookup_to_c (ml_table_webkit_web_view_view_mode, key)

/* webkit_selection_affinity : tags and macros */
#define MLTAG_DOWNSTREAM	((value)(195425890*2+1))
#define MLTAG_UPSTREAM	((value)(-1071333093*2+1))

extern const lookup_info ml_table_webkit_selection_affinity[];
#define Val_webkit_selection_affinity(data) ml_lookup_from_c (ml_table_webkit_selection_affinity, data)
#define Webkit_selection_affinity_val(key) ml_lookup_to_c (ml_table_webkit_selection_affinity, key)

/* webkit_insert_action : tags and macros */
#define MLTAG_DROPPED	((value)(113047520*2+1))
#define MLTAG_PASTED	((value)(548749745*2+1))
#define MLTAG_TYPED	((value)(414820426*2+1))

extern const lookup_info ml_table_webkit_insert_action[];
#define Val_webkit_insert_action(data) ml_lookup_from_c (ml_table_webkit_insert_action, data)
#define Webkit_insert_action_val(key) ml_lookup_to_c (ml_table_webkit_insert_action, key)

