(** webkit_web_view enums *)

open Gpointer

type webkit_navigation_response = [ `DOWNLOAD | `IGNORE | `ACCEPT ]
type webkit_web_view_target_info =
  [ `NETSCAPE_URL | `URI_LIST | `IMAGE | `TEXT | `HTML ]
type webkit_web_view_view_mode =
  [ `MINIMIZED | `MAXIMIZED | `FULLSCREEN | `FLOATING | `WINDOWED ]
type webkit_selection_affinity = [ `DOWNSTREAM | `UPSTREAM ]
type webkit_insert_action = [ `DROPPED | `PASTED | `TYPED ]

(**/**)

external _get_tables : unit ->
    webkit_navigation_response variant_table
  * webkit_web_view_target_info variant_table
  * webkit_web_view_view_mode variant_table
  * webkit_selection_affinity variant_table
  * webkit_insert_action variant_table
  = "ml_webkit_web_view_get_tables"


let webkit_navigation_response, webkit_web_view_target_info,
    webkit_web_view_view_mode, webkit_selection_affinity,
    webkit_insert_action = _get_tables ()

let webkit_navigation_response_conv = Gobject.Data.enum webkit_navigation_response
let webkit_web_view_target_info_conv = Gobject.Data.enum webkit_web_view_target_info
let webkit_web_view_view_mode_conv = Gobject.Data.enum webkit_web_view_view_mode
let webkit_selection_affinity_conv = Gobject.Data.enum webkit_selection_affinity
let webkit_insert_action_conv = Gobject.Data.enum webkit_insert_action
