open GObj
open Gobject
open Gtk
open GtkWebBackForwardList
open OGtkWebBackForwardListProps

class web_back_forward_list_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  method go_forward = WebBackForwardList.go_forward obj_
  method go_back = WebBackForwardList.go_back obj_
  method contains_item = WebBackForwardList.contains_item obj_
  method go_to_item = WebBackForwardList.go_to_item obj_
  method get_back_item = WebBackForwardList.get_back_item obj_
  method get_current_item = WebBackForwardList.get_current_item obj_
  method get_forward_item = WebBackForwardList.get_forward_item obj_
  method get_nth_item = WebBackForwardList.get_nth_item obj_
  method get_back_length = WebBackForwardList.get_back_length obj_
  method get_forward_length = WebBackForwardList.get_forward_length obj_
  method get_limit = WebBackForwardList.get_limit obj_
  method set_limit = WebBackForwardList.set_limit obj_
  method add_item = WebBackForwardList.add_item obj_
  method clear = WebBackForwardList.clear obj_
end

class web_back_forward_list obj_ = object(self)
  method as_webbackforwardlist : GtkWebkitTypes.webbackforwardlist obj = obj_
  inherit web_back_forward_list_skel obj_
end

let web_back_forward_list_with_web_view web_view =
  new web_back_forward_list (WebBackForwardList.new_with_web_view web_view)

let wrap_webbackforwardlist obj = new web_back_forward_list (unsafe_cast obj)
let unwrap_webbackforwardlist obj = unsafe_cast obj#as_webbackforwardlist
let conv_webbackforwardlist =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webbackforwardlist c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webbackforwardlist c))) }

