open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebBackForwardList = struct
  let cast w : webbackforwardlist obj = try_cast w "WebkitWebBackForwardList"
  let create pl : webbackforwardlist obj =
    Gobject.unsafe_create "WebkitWebBackForwardList" pl
end

