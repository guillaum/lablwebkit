open Gobject
open Data
open GtkWebkitTypes
module WebBackForwardList = struct
  external new_with_web_view : webview Gtk.obj -> webbackforwardlist Gtk.obj = "ml_webkit_web_back_forward_list_new_with_web_view"
  external go_forward : webbackforwardlist Gtk.obj -> unit = "ml_webkit_web_back_forward_list_go_forward"
  external go_back : webbackforwardlist Gtk.obj -> unit = "ml_webkit_web_back_forward_list_go_back"
  external contains_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj -> bool = "ml_webkit_web_back_forward_list_contains_item"
  external go_to_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj -> unit = "ml_webkit_web_back_forward_list_go_to_item"
  external get_back_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj = "ml_webkit_web_back_forward_list_get_back_item"
  external get_current_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj = "ml_webkit_web_back_forward_list_get_current_item"
  external get_forward_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj = "ml_webkit_web_back_forward_list_get_forward_item"
  external get_nth_item : webbackforwardlist Gtk.obj -> int -> webhistoryitem Gtk.obj = "ml_webkit_web_back_forward_list_get_nth_item"
  external get_back_length : webbackforwardlist Gtk.obj -> int = "ml_webkit_web_back_forward_list_get_back_length"
  external get_forward_length : webbackforwardlist Gtk.obj -> int = "ml_webkit_web_back_forward_list_get_forward_length"
  external get_limit : webbackforwardlist Gtk.obj -> int = "ml_webkit_web_back_forward_list_get_limit"
  external set_limit : webbackforwardlist Gtk.obj -> int -> unit = "ml_webkit_web_back_forward_list_set_limit"
  external add_item : webbackforwardlist Gtk.obj -> webhistoryitem Gtk.obj -> unit = "ml_webkit_web_back_forward_list_add_item"
  external clear : webbackforwardlist Gtk.obj -> unit = "ml_webkit_web_back_forward_list_clear"
end