#include "ml_gtkWebkit.h"
#include "ml_gtkNetworkRequest_tags_c.h"



ML_0(webkit_network_request_get_type, Val_gtype)

ML_1(webkit_network_request_new, String_val, Val_webkit_network_request)

ML_2(webkit_network_request_set_uri, WebkitNetworkRequest_val, String_val, Unit)

ML_1(webkit_network_request_get_uri, WebkitNetworkRequest_val, Val_string)

