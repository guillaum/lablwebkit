open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module NetworkRequest = struct
  let cast w : networkrequest obj = try_cast w "WebkitNetworkRequest"
  module P = struct
    let uri : ([>`networkrequest],_) property = {name="uri"; conv=string}
  end
  let create pl : networkrequest obj =
    Gobject.unsafe_create "WebkitNetworkRequest" pl
  let make_params ~cont pl ?uri = let pl = (may_cons P.uri uri pl) in cont pl
end

