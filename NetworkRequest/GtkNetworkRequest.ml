open Gobject
open Data
open GtkWebkitTypes
module NetworkRequest = struct
  include GtkNetworkRequestProps.NetworkRequest
  external create : string -> networkrequest Gtk.obj = "ml_webkit_network_request_new"
  
end