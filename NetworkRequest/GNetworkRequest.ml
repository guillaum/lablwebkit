open GObj
open Gobject
open Gtk
open GtkNetworkRequest
open OGtkNetworkRequestProps

class network_request_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit network_request_props
end

class network_request obj_ = object(self)
  method as_networkrequest : GtkWebkitTypes.networkrequest obj = obj_
  inherit network_request_skel obj_
end

let network_request uri =
  new network_request (NetworkRequest.create uri)

let wrap_networkrequest obj = new network_request (unsafe_cast obj)
let unwrap_networkrequest obj = unsafe_cast obj#as_networkrequest
let conv_networkrequest =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_networkrequest c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_networkrequest c))) }

