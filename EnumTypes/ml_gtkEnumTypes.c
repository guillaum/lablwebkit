#include "ml_gtkWebkit.h"
#include "ml_gtkEnumTypes_tags_c.h"



ML_0(webkit_download_status_get_type, Val_gtype)

ML_0(webkit_download_error_get_type, Val_gtype)

ML_0(webkit_network_error_get_type, Val_gtype)

ML_0(webkit_policy_error_get_type, Val_gtype)

ML_0(webkit_plugin_error_get_type, Val_gtype)

ML_0(webkit_cache_model_get_type, Val_gtype)

ML_0(webkit_hit_test_result_context_get_type, Val_gtype)

ML_0(webkit_load_status_get_type, Val_gtype)

ML_0(webkit_web_navigation_reason_get_type, Val_gtype)

ML_0(webkit_editing_behavior_get_type, Val_gtype)

ML_0(webkit_navigation_response_get_type, Val_gtype)

ML_0(webkit_web_view_target_info_get_type, Val_gtype)

ML_0(webkit_web_view_view_mode_get_type, Val_gtype)

ML_0(webkit_selection_affinity_get_type, Val_gtype)

ML_0(webkit_insert_action_get_type, Val_gtype)

