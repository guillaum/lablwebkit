#include "EnumTypes/ml_gtkEnumTypes_tags_h.h"

#define WebkitEnumTypes_val(val) check_cast(WEBKIT_ENUM_TYPES,val)
#define Val_webkit_enum_types(val) Val_GtkAny(val)
