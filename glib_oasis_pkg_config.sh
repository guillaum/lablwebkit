#!/bin/sh

set -eu

CFLAGS="-I$(pwd) $(pkg-config --cflags $1) $(ocamlfind query -i-format lablgtk2)"
LFLAGS="$(pkg-config --libs $1)"

TMPFILE=$(mktemp)
#TMPFILE=$(mktemp -t "tmp.XXX")

sed -e 's%@CCOPT@%'"$CFLAGS"'%' < myocamlbuild.ml > $TMPFILE
sed -e 's%@CCLIB@%'"$LFLAGS"'%' < $TMPFILE > myocamlbuild.ml

rm $TMPFILE
