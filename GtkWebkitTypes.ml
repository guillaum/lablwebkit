open Gobject
open Gpointer


type webbackforwardlist = [ `webbackforwardlist | `gtk ]


type download = [ `download | `gtk ]


(** webkit_download enums *)

open Gpointer

type webkit_download_status =
  [ `FINISHED | `CANCELLED | `STARTED | `CREATED | `ERROR ]
type webkit_download_error = [ `NETWORK | `DESTINATION | `CANCELLED_BY_USER ]

(**/**)

external _get_tables : unit ->
    webkit_download_status variant_table
  * webkit_download_error variant_table
  = "ml_webkit_download_get_tables"


let webkit_download_status, webkit_download_error = _get_tables ()

let webkit_download_status_conv = Gobject.Data.enum webkit_download_status
let webkit_download_error_conv = Gobject.Data.enum webkit_download_error
type webpolicydecision = [ `webpolicydecision | `gtk ]


type networkresponse = [ `networkresponse | `gtk ]


type viewportattributes = [ `viewportattributes | `gtk ]


type websettings = [ `websettings | `gtk ]


(** webkit_web_settings enums *)

open Gpointer

type webkit_editing_behavior = [ `UNIX | `WINDOWS | `MAC ]

(**/**)

external _get_tables : unit ->
    webkit_editing_behavior variant_table
  = "ml_webkit_web_settings_get_tables"


let webkit_editing_behavior = _get_tables ()

let webkit_editing_behavior_conv = Gobject.Data.enum webkit_editing_behavior
type webinspector = [ `webinspector | `gtk ]


type webview = [ Gtk.container | `webview | `gtk ]


(** webkit_web_view enums *)

open Gpointer

type webkit_navigation_response = [ `DOWNLOAD | `IGNORE | `ACCEPT ]
type webkit_web_view_target_info =
  [ `NETSCAPE_URL | `URI_LIST | `IMAGE | `TEXT | `HTML ]
type webkit_web_view_view_mode =
  [ `MINIMIZED | `MAXIMIZED | `FULLSCREEN | `FLOATING | `WINDOWED ]
type webkit_selection_affinity = [ `DOWNSTREAM | `UPSTREAM ]
type webkit_insert_action = [ `DROPPED | `PASTED | `TYPED ]

(**/**)

external _get_tables : unit ->
    webkit_navigation_response variant_table
  * webkit_web_view_target_info variant_table
  * webkit_web_view_view_mode variant_table
  * webkit_selection_affinity variant_table
  * webkit_insert_action variant_table
  = "ml_webkit_web_view_get_tables"


let webkit_navigation_response, webkit_web_view_target_info,
    webkit_web_view_view_mode, webkit_selection_affinity,
    webkit_insert_action = _get_tables ()

let webkit_navigation_response_conv = Gobject.Data.enum webkit_navigation_response
let webkit_web_view_target_info_conv = Gobject.Data.enum webkit_web_view_target_info
let webkit_web_view_view_mode_conv = Gobject.Data.enum webkit_web_view_view_mode
let webkit_selection_affinity_conv = Gobject.Data.enum webkit_selection_affinity
let webkit_insert_action_conv = Gobject.Data.enum webkit_insert_action
type favicondatabase = [ `favicondatabase | `gtk ]


type webresource = [ `webresource | `gtk ]


type networkrequest = [ `networkrequest | `gtk ]


type icondatabase = [ `icondatabase | `gtk ]


type webhistoryitem = [ `webhistoryitem | `gtk ]


type hittestresult = [ `hittestresult | `gtk ]


(** webkit_hit_test_result enums *)

open Gpointer

type webkit_hit_test_result_context =
  [ `EDITABLE | `SELECTION | `MEDIA | `IMAGE | `LINK | `DOCUMENT ]

(**/**)

external _get_tables : unit ->
    webkit_hit_test_result_context variant_table
  = "ml_webkit_hit_test_result_get_tables"


let webkit_hit_test_result_context = _get_tables ()

let webkit_hit_test_result_context_conv = Gobject.Data.enum webkit_hit_test_result_context
type geolocationpolicydecision = [ `geolocationpolicydecision | `gtk ]


type webnavigationaction = [ `webnavigationaction | `gtk ]


(** webkit_web_navigation_action enums *)

open Gpointer

type webkit_web_navigation_reason =
  [ `OTHER | `FORM_RESUBMITTED | `RELOAD | `BACK_FORWARD | `FORM_SUBMITTED
  | `LINK_CLICKED ]

(**/**)

external _get_tables : unit ->
    webkit_web_navigation_reason variant_table
  = "ml_webkit_web_navigation_action_get_tables"


let webkit_web_navigation_reason = _get_tables ()

let webkit_web_navigation_reason_conv = Gobject.Data.enum webkit_web_navigation_reason
type securityorigin = [ `securityorigin | `gtk ]


type webdatabase = [ `webdatabase | `gtk ]


type webwindowfeatures = [ `webwindowfeatures | `gtk ]


type webplugindatabase = [ `webplugindatabase | `gtk ]


type webdatasource = [ `webdatasource | `gtk ]


type webplugin = [ `webplugin | `gtk ]


type webframe = [ `webframe | `gtk ]


(** webkit_web_frame enums *)

open Gpointer

type webkit_load_status =
  [ `FAILED | `FIRST_VISUALLY_NON_EMPTY_LAYOUT | `FINISHED | `COMMITTED
  | `PROVISIONAL ]

(**/**)

external _get_tables : unit ->
    webkit_load_status variant_table
  = "ml_webkit_web_frame_get_tables"


let webkit_load_status = _get_tables ()

let webkit_load_status_conv = Gobject.Data.enum webkit_load_status
