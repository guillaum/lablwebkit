open Gobject
open Data
open GtkWebkitTypes
module WebFrame = struct
  include GtkWebFrameProps.WebFrame
  external create : webview Gtk.obj -> webframe Gtk.obj = "ml_webkit_web_frame_new"
  external get_web_view : webframe Gtk.obj -> webview Gtk.obj = "ml_webkit_web_frame_get_web_view"
  external get_parent : webframe Gtk.obj -> webframe Gtk.obj = "ml_webkit_web_frame_get_parent"
  external load_uri : webframe Gtk.obj -> string -> unit = "ml_webkit_web_frame_load_uri"
  external load_string : webframe Gtk.obj -> string -> string -> string -> string -> unit = "ml_webkit_web_frame_load_string"
  external load_alternate_string : webframe Gtk.obj -> string -> string -> string -> unit = "ml_webkit_web_frame_load_alternate_string"
  external load_request : webframe Gtk.obj -> networkrequest Gtk.obj -> unit = "ml_webkit_web_frame_load_request"
  external stop_loading : webframe Gtk.obj -> unit = "ml_webkit_web_frame_stop_loading"
  external reload : webframe Gtk.obj -> unit = "ml_webkit_web_frame_reload"
  external find_frame : webframe Gtk.obj -> string -> webframe Gtk.obj = "ml_webkit_web_frame_find_frame"
  external print : webframe Gtk.obj -> unit = "ml_webkit_web_frame_print"
  external get_data_source : webframe Gtk.obj -> webdatasource Gtk.obj = "ml_webkit_web_frame_get_data_source"
  external get_provisional_data_source : webframe Gtk.obj -> webdatasource Gtk.obj = "ml_webkit_web_frame_get_provisional_data_source"
  external get_security_origin : webframe Gtk.obj -> securityorigin Gtk.obj = "ml_webkit_web_frame_get_security_origin"
  external get_network_response : webframe Gtk.obj -> networkresponse Gtk.obj = "ml_webkit_web_frame_get_network_response"
  external replace_selection : webframe Gtk.obj -> char -> unit = "ml_webkit_web_frame_replace_selection"
end