open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebFrame = struct
  let cast w : webframe obj = try_cast w "WebkitWebFrame"
  module P = struct
    let name : ([>`webframe],_) property = {name="name"; conv=string}
    let title : ([>`webframe],_) property = {name="title"; conv=string}
    let uri : ([>`webframe],_) property = {name="uri"; conv=string}
    let load_status : ([>`webframe],_) property =
      {name="load-status"; conv=webkit_load_status_conv}
    let horizontal_scrollbar_policy : ([>`webframe],_) property =
      {name="horizontal-scrollbar-policy"; conv=GtkEnums.policy_type_conv}
    let vertical_scrollbar_policy : ([>`webframe],_) property =
      {name="vertical-scrollbar-policy"; conv=GtkEnums.policy_type_conv}
  end
  module S = struct
    open GtkSignal
    let cleared = {name="cleared"; classe=`webframe; marshaller=marshal_unit}
    let load_committed =
      {name="load_committed"; classe=`webframe; marshaller=marshal_unit}
    let load_done =
      {name="load_done"; classe=`webframe; marshaller=fun f ->
       marshal1 boolean "WebkitWebFrame::load_done" f}
    let title_changed =
      {name="title_changed"; classe=`webframe; marshaller=fun f ->
       marshal1 string "WebkitWebFrame::title_changed" f}
    let hovering_over_link =
      {name="hovering_over_link"; classe=`webframe; marshaller=fun f ->
       marshal2 string string "WebkitWebFrame::hovering_over_link" f}
    let scrollbars_policy_changed =
      {name="scrollbars_policy_changed"; classe=`webframe;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let resource_request_starting =
      {name="resource_request_starting"; classe=`webframe;
       marshaller=fun f ->
       marshal3 GWebResource.conv_webresource
         GNetworkRequest.conv_networkrequest
         GNetworkResponse.conv_networkresponse
         "WebkitWebFrame::resource_request_starting" f}
    let resource_response_received =
      {name="resource_response_received"; classe=`webframe;
       marshaller=fun f ->
       marshal2 GWebResource.conv_webresource
         GNetworkResponse.conv_networkresponse
         "WebkitWebFrame::resource_response_received" f}
    let resource_load_finished =
      {name="resource_load_finished"; classe=`webframe; marshaller=fun f ->
       marshal1 GWebResource.conv_webresource
         "WebkitWebFrame::resource_load_finished" f}
    let resource_content_length_received =
      {name="resource_content_length_received"; classe=`webframe;
       marshaller=fun f ->
       marshal2 GWebResource.conv_webresource int
         "WebkitWebFrame::resource_content_length_received" f}
  end
  let create pl : webframe obj = Gobject.unsafe_create "WebkitWebFrame" pl
end

