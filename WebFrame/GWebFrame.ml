open GObj
open Gobject
open Gtk
open GtkWebFrame
open OGtkWebFrameProps

class web_frame_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_frame_props
  method get_web_view = WebFrame.get_web_view obj_
  method get_parent = WebFrame.get_parent obj_
  method load_uri = WebFrame.load_uri obj_
  method load_string = WebFrame.load_string obj_
  method load_alternate_string = WebFrame.load_alternate_string obj_
  method load_request = WebFrame.load_request obj_
  method stop_loading = WebFrame.stop_loading obj_
  method reload = WebFrame.reload obj_
  method find_frame = WebFrame.find_frame obj_
  method print = WebFrame.print obj_
  method get_data_source = WebFrame.get_data_source obj_
  method get_provisional_data_source = WebFrame.get_provisional_data_source obj_
  method get_security_origin = WebFrame.get_security_origin obj_
  method get_network_response = WebFrame.get_network_response obj_
  method replace_selection = WebFrame.replace_selection obj_
end

class web_frame_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit web_frame_sigs
end

class web_frame obj_ = object(self)
  method as_webframe : GtkWebkitTypes.webframe obj = obj_
  inherit web_frame_skel obj_
  method connect = new web_frame_signals obj_
end

let web_frame web_view =
  new web_frame (WebFrame.create web_view)

let wrap_webframe obj = new web_frame (unsafe_cast obj)
let unwrap_webframe obj = unsafe_cast obj#as_webframe
let conv_webframe =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webframe c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webframe c))) }

