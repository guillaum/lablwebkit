/* webkit_load_status : tags and macros */
#define MLTAG_FAILED	((value)(444690877*2+1))
#define MLTAG_FIRST_VISUALLY_NON_EMPTY_LAYOUT	((value)(-501917807*2+1))
#define MLTAG_FINISHED	((value)(-292281486*2+1))
#define MLTAG_COMMITTED	((value)(-415927780*2+1))
#define MLTAG_PROVISIONAL	((value)(-188804128*2+1))

extern const lookup_info ml_table_webkit_load_status[];
#define Val_webkit_load_status(data) ml_lookup_from_c (ml_table_webkit_load_status, data)
#define Webkit_load_status_val(key) ml_lookup_to_c (ml_table_webkit_load_status, key)

