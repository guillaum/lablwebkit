open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebFrameProps

class virtual web_frame_props = object
  val virtual obj : _ obj
  method name = get WebFrame.P.name obj
  method title = get WebFrame.P.title obj
  method uri = get WebFrame.P.uri obj
  method load_status = get WebFrame.P.load_status obj
  method horizontal_scrollbar_policy =
    get WebFrame.P.horizontal_scrollbar_policy obj
  method vertical_scrollbar_policy =
    get WebFrame.P.vertical_scrollbar_policy obj
end

class virtual web_frame_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method name = self#notify WebFrame.P.name
  method title = self#notify WebFrame.P.title
  method uri = self#notify WebFrame.P.uri
  method load_status = self#notify WebFrame.P.load_status
  method horizontal_scrollbar_policy =
    self#notify WebFrame.P.horizontal_scrollbar_policy
  method vertical_scrollbar_policy =
    self#notify WebFrame.P.vertical_scrollbar_policy
end

class virtual web_frame_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method cleared = self#connect WebFrame.S.cleared
  method load_committed = self#connect WebFrame.S.load_committed
  method load_done = self#connect WebFrame.S.load_done
  method title_changed = self#connect WebFrame.S.title_changed
  method hovering_over_link = self#connect WebFrame.S.hovering_over_link
  method scrollbars_policy_changed =
    self#connect WebFrame.S.scrollbars_policy_changed
  method resource_request_starting =
    self#connect WebFrame.S.resource_request_starting
  method resource_response_received =
    self#connect WebFrame.S.resource_response_received
  method resource_load_finished =
    self#connect WebFrame.S.resource_load_finished
  method resource_content_length_received =
    self#connect WebFrame.S.resource_content_length_received
  method notify_name ~callback = self#notify WebFrame.P.name ~callback
  method notify_title ~callback = self#notify WebFrame.P.title ~callback
  method notify_uri ~callback = self#notify WebFrame.P.uri ~callback
  method notify_load_status ~callback =
    self#notify WebFrame.P.load_status ~callback
  method notify_horizontal_scrollbar_policy ~callback =
    self#notify WebFrame.P.horizontal_scrollbar_policy ~callback
  method notify_vertical_scrollbar_policy ~callback =
    self#notify WebFrame.P.vertical_scrollbar_policy ~callback
end

