(** webkit_web_frame enums *)

open Gpointer

type webkit_load_status =
  [ `FAILED | `FIRST_VISUALLY_NON_EMPTY_LAYOUT | `FINISHED | `COMMITTED
  | `PROVISIONAL ]

(**/**)

external _get_tables : unit ->
    webkit_load_status variant_table
  = "ml_webkit_web_frame_get_tables"


let webkit_load_status = _get_tables ()

let webkit_load_status_conv = Gobject.Data.enum webkit_load_status
