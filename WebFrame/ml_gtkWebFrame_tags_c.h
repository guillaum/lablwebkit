/* webkit_load_status : conversion table */
const lookup_info ml_table_webkit_load_status[] = {
  { 0, 5 },
  { MLTAG_FIRST_VISUALLY_NON_EMPTY_LAYOUT, WEBKIT_LOAD_FIRST_VISUALLY_NON_EMPTY_LAYOUT },
  { MLTAG_COMMITTED, WEBKIT_LOAD_COMMITTED },
  { MLTAG_FINISHED, WEBKIT_LOAD_FINISHED },
  { MLTAG_PROVISIONAL, WEBKIT_LOAD_PROVISIONAL },
  { MLTAG_FAILED, WEBKIT_LOAD_FAILED },
};

CAMLprim value ml_webkit_web_frame_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_load_status,
  };
  return (value)ml_lookup_tables[0];}
