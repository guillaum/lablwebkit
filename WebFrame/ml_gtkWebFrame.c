#include "ml_gtkWebkit.h"
#include "ml_gtkWebFrame_tags_c.h"

#define WebkitLoadStatus_val Webkit_load_status_val

ML_0(webkit_web_frame_get_type, Val_gtype)

ML_1(webkit_web_frame_new, WebkitWebView_val, Val_webkit_web_frame)

ML_1(webkit_web_frame_get_web_view, WebkitWebFrame_val, Val_webkit_web_view)

ML_1(webkit_web_frame_get_name, WebkitWebFrame_val, Val_string)

ML_1(webkit_web_frame_get_title, WebkitWebFrame_val, Val_string)

ML_1(webkit_web_frame_get_uri, WebkitWebFrame_val, Val_string)

ML_1(webkit_web_frame_get_parent, WebkitWebFrame_val, Val_webkit_web_frame)

ML_2(webkit_web_frame_load_uri, WebkitWebFrame_val, String_val, Unit)

ML_5(webkit_web_frame_load_string, WebkitWebFrame_val, String_val, String_val, String_val, String_val, Unit)

ML_4(webkit_web_frame_load_alternate_string, WebkitWebFrame_val, String_val, String_val, String_val, Unit)

ML_2(webkit_web_frame_load_request, WebkitWebFrame_val, WebkitNetworkRequest_val, Unit)

ML_1(webkit_web_frame_stop_loading, WebkitWebFrame_val, Unit)

ML_1(webkit_web_frame_reload, WebkitWebFrame_val, Unit)

ML_2(webkit_web_frame_find_frame, WebkitWebFrame_val, String_val, Val_webkit_web_frame)

ML_1(webkit_web_frame_print, WebkitWebFrame_val, Unit)

ML_1(webkit_web_frame_get_load_status, WebkitWebFrame_val, Val_webkit_load_status)

ML_1(webkit_web_frame_get_horizontal_scrollbar_policy, WebkitWebFrame_val, Val_gtk_policy_type)

ML_1(webkit_web_frame_get_vertical_scrollbar_policy, WebkitWebFrame_val, Val_gtk_policy_type)

ML_1(webkit_web_frame_get_data_source, WebkitWebFrame_val, Val_webkit_web_data_source)

ML_1(webkit_web_frame_get_provisional_data_source, WebkitWebFrame_val, Val_webkit_web_data_source)

ML_1(webkit_web_frame_get_security_origin, WebkitWebFrame_val, Val_webkit_security_origin)

ML_1(webkit_web_frame_get_network_response, WebkitWebFrame_val, Val_webkit_network_response)

ML_2(webkit_web_frame_replace_selection, WebkitWebFrame_val, Char_val, Unit)

