#include "ml_gtkWebkit.h"
#include "ml_gtkWebInspector_tags_c.h"



ML_0(webkit_web_inspector_get_type, Val_gtype)

ML_1(webkit_web_inspector_get_web_view, WebkitWebInspector_val, Val_webkit_web_view)

ML_1(webkit_web_inspector_get_inspected_uri, WebkitWebInspector_val, Val_string)

ML_3(webkit_web_inspector_inspect_coordinates, WebkitWebInspector_val, Double_val, Double_val, Unit)

ML_1(webkit_web_inspector_show, WebkitWebInspector_val, Unit)

ML_1(webkit_web_inspector_close, WebkitWebInspector_val, Unit)

