open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebInspectorProps

class virtual web_inspector_props = object
  val virtual obj : _ obj
  method set_javascript_profiling_enabled =
    set WebInspector.P.javascript_profiling_enabled obj
  method set_timeline_profiling_enabled =
    set WebInspector.P.timeline_profiling_enabled obj
  method inspected_uri = get WebInspector.P.inspected_uri obj
  method javascript_profiling_enabled =
    get WebInspector.P.javascript_profiling_enabled obj
  method timeline_profiling_enabled =
    get WebInspector.P.timeline_profiling_enabled obj
end

class virtual web_inspector_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method inspected_uri = self#notify WebInspector.P.inspected_uri
  method javascript_profiling_enabled =
    self#notify WebInspector.P.javascript_profiling_enabled
  method timeline_profiling_enabled =
    self#notify WebInspector.P.timeline_profiling_enabled
end

class virtual web_inspector_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method show_window = self#connect WebInspector.S.show_window
  method attach_window = self#connect WebInspector.S.attach_window
  method detach_window = self#connect WebInspector.S.detach_window
  method close_window = self#connect WebInspector.S.close_window
  method finished = self#connect WebInspector.S.finished
  method notify_inspected_uri ~callback =
    self#notify WebInspector.P.inspected_uri ~callback
  method notify_javascript_profiling_enabled ~callback =
    self#notify WebInspector.P.javascript_profiling_enabled ~callback
  method notify_timeline_profiling_enabled ~callback =
    self#notify WebInspector.P.timeline_profiling_enabled ~callback
end

