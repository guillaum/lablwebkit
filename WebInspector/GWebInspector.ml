open GObj
open Gobject
open Gtk
open GtkWebInspector
open OGtkWebInspectorProps

class web_inspector_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_inspector_props
  method get_web_view = WebInspector.get_web_view obj_
  method inspect_coordinates = WebInspector.inspect_coordinates obj_
  method show = WebInspector.show obj_
  method close = WebInspector.close obj_
end

class web_inspector_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit web_inspector_sigs
end

class web_inspector obj_ = object(self)
  method as_webinspector : GtkWebkitTypes.webinspector obj = obj_
  inherit web_inspector_skel obj_
  method connect = new web_inspector_signals obj_
end



let wrap_webinspector obj = new web_inspector (unsafe_cast obj)
let unwrap_webinspector obj = unsafe_cast obj#as_webinspector
let conv_webinspector =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webinspector c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webinspector c))) }

