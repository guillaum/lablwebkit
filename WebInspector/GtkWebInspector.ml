open Gobject
open Data
open GtkWebkitTypes
module WebInspector = struct
  include GtkWebInspectorProps.WebInspector
  
  external get_web_view : webinspector Gtk.obj -> webview Gtk.obj = "ml_webkit_web_inspector_get_web_view"
  external inspect_coordinates : webinspector Gtk.obj -> float -> float -> unit = "ml_webkit_web_inspector_inspect_coordinates"
  external show : webinspector Gtk.obj -> unit = "ml_webkit_web_inspector_show"
  external close : webinspector Gtk.obj -> unit = "ml_webkit_web_inspector_close"
end