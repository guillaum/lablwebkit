#include "WebInspector/ml_gtkWebInspector_tags_h.h"

#define WebkitWebInspector_val(val) check_cast(WEBKIT_WEB_INSPECTOR,val)
#define Val_webkit_web_inspector(val) Val_GtkAny(val)
