open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebInspector = struct
  let cast w : webinspector obj = try_cast w "WebkitWebInspector"
  module P = struct
    let inspected_uri : ([>`webinspector],_) property =
      {name="inspected-uri"; conv=string}
    let javascript_profiling_enabled : ([>`webinspector],_) property =
      {name="javascript-profiling-enabled"; conv=boolean}
    let timeline_profiling_enabled : ([>`webinspector],_) property =
      {name="timeline-profiling-enabled"; conv=boolean}
  end
  module S = struct
    open GtkSignal
    let show_window =
      {name="show_window"; classe=`webinspector;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let attach_window =
      {name="attach_window"; classe=`webinspector;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let detach_window =
      {name="detach_window"; classe=`webinspector;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let close_window =
      {name="close_window"; classe=`webinspector;
       marshaller=fun f -> marshal0_ret ~ret:boolean f}
    let finished =
      {name="finished"; classe=`webinspector; marshaller=marshal_unit}
  end
  let create pl : webinspector obj =
    Gobject.unsafe_create "WebkitWebInspector" pl
  let make_params ~cont pl ?javascript_profiling_enabled
      ?timeline_profiling_enabled =
    let pl = (
      may_cons P.javascript_profiling_enabled javascript_profiling_enabled (
      may_cons P.timeline_profiling_enabled timeline_profiling_enabled pl)) in
    cont pl
end

