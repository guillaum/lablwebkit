#include "ml_gtkWebkit.h"
#include "ml_gtkSpellChecker_tags_c.h"



ML_0(webkit_spell_checker_get_type, Val_gtype)

ML_4(webkit_spell_checker_check_spelling_of_string, WebkitSpellChecker_val, Char_val, Int_val, Int_val, Unit)

ML_3(webkit_spell_checker_get_guesses_for_word, WebkitSpellChecker_val, Char_val, Char_val, Val_char)

ML_2(webkit_spell_checker_update_spell_checking_languages, WebkitSpellChecker_val, Char_val, Unit)

ML_2(webkit_spell_checker_get_autocorrect_suggestions_for_misspelled_word, WebkitSpellChecker_val, Char_val, Val_char)

ML_2(webkit_spell_checker_learn_word, WebkitSpellChecker_val, Char_val, Unit)

ML_2(webkit_spell_checker_ignore_word, WebkitSpellChecker_val, Char_val, Unit)

