open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebDatabaseProps

class virtual web_database_props = object
  val virtual obj : _ obj
  method security_origin = get WebDatabase.P.security_origin obj
  method name = get WebDatabase.P.name obj
  method display_name = get WebDatabase.P.display_name obj
  method expected_size = get WebDatabase.P.expected_size obj
  method size = get WebDatabase.P.size obj
  method filename = get WebDatabase.P.filename obj
end

class virtual web_database_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method security_origin = self#notify WebDatabase.P.security_origin
  method name = self#notify WebDatabase.P.name
  method display_name = self#notify WebDatabase.P.display_name
  method expected_size = self#notify WebDatabase.P.expected_size
  method size = self#notify WebDatabase.P.size
  method filename = self#notify WebDatabase.P.filename
end

