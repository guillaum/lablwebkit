open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebDatabase = struct
  let cast w : webdatabase obj = try_cast w "WebkitWebDatabase"
  module P = struct
    let security_origin : ([>`webdatabase],_) property =
      {name="security-origin"; conv=GSecurityOrigin.conv_securityorigin}
    let name : ([>`webdatabase],_) property = {name="name"; conv=string}
    let display_name : ([>`webdatabase],_) property =
      {name="display-name"; conv=string}
    let expected_size : ([>`webdatabase],_) property =
      {name="expected-size"; conv=uint64}
    let size : ([>`webdatabase],_) property = {name="size"; conv=uint64}
    let filename : ([>`webdatabase],_) property =
      {name="filename"; conv=string}
  end
  let create ?security_origin ?name pl : webdatabase obj =
    let pl = (
      may_cons P.security_origin security_origin (
      may_cons P.name name pl)) in
    Gobject.unsafe_create "WebkitWebDatabase" pl
end

