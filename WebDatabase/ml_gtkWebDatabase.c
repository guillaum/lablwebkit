#include "ml_gtkWebkit.h"
#include "ml_gtkWebDatabase_tags_c.h"



ML_0(webkit_web_database_get_type, Val_gtype)

ML_1(webkit_web_database_get_security_origin, WebkitWebDatabase_val, Val_webkit_security_origin)

ML_1(webkit_web_database_get_name, WebkitWebDatabase_val, Val_string)

ML_1(webkit_web_database_get_display_name, WebkitWebDatabase_val, Val_string)

ML_1(webkit_web_database_get_expected_size, WebkitWebDatabase_val, Val_uint64_t)

ML_1(webkit_web_database_get_size, WebkitWebDatabase_val, Val_uint64_t)

ML_1(webkit_web_database_get_filename, WebkitWebDatabase_val, Val_string)

ML_1(webkit_web_database_remove, WebkitWebDatabase_val, Unit)

ML_0(webkit_remove_all_web_databases, Unit)

ML_0(webkit_get_web_database_directory_path, Val_string)

ML_1(webkit_set_web_database_directory_path, String_val, Unit)

ML_0(webkit_get_default_web_database_quota, Val_uint64_t)

ML_1(webkit_set_default_web_database_quota, Uint64_t_val, Unit)

