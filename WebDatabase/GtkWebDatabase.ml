open Gobject
open Data
open GtkWebkitTypes
module WebDatabase = struct
  include GtkWebDatabaseProps.WebDatabase
  
  external remove : webdatabase Gtk.obj -> unit = "ml_webkit_web_database_remove"
  external remove_all_web_databases : unit -> unit = "ml_webkit_remove_all_web_databases"
  external get_web_database_directory_path : unit -> string = "ml_webkit_get_web_database_directory_path"
  external set_web_database_directory_path : string -> unit = "ml_webkit_set_web_database_directory_path"
  external get_default_web_database_quota : unit -> Int64.t = "ml_webkit_get_default_web_database_quota"
  external set_default_web_database_quota : Int64.t -> unit = "ml_webkit_set_default_web_database_quota"
end