open GObj
open Gobject
open Gtk
open GtkWebDatabase
open OGtkWebDatabaseProps

class web_database_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_database_props
  method remove = WebDatabase.remove obj_
end

class web_database obj_ = object(self)
  method as_webdatabase : GtkWebkitTypes.webdatabase obj = obj_
  inherit web_database_skel obj_
end



let wrap_webdatabase obj = new web_database (unsafe_cast obj)
let unwrap_webdatabase obj = unsafe_cast obj#as_webdatabase
let conv_webdatabase =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webdatabase c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webdatabase c))) }

