#include "Error/ml_gtkError_tags_h.h"

#define WebkitError_val(val) check_cast(WEBKIT_ERROR,val)
#define Val_webkit_error(val) Val_GtkAny(val)
