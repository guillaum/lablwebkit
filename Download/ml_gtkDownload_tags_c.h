/* webkit_download_status : conversion table */
const lookup_info ml_table_webkit_download_status[] = {
  { 0, 5 },
  { MLTAG_FINISHED, WEBKIT_DOWNLOAD_STATUS_FINISHED },
  { MLTAG_ERROR, WEBKIT_DOWNLOAD_STATUS_ERROR },
  { MLTAG_CREATED, WEBKIT_DOWNLOAD_STATUS_CREATED },
  { MLTAG_CANCELLED, WEBKIT_DOWNLOAD_STATUS_CANCELLED },
  { MLTAG_STARTED, WEBKIT_DOWNLOAD_STATUS_STARTED },
};

/* webkit_download_error : conversion table */
const lookup_info ml_table_webkit_download_error[] = {
  { 0, 3 },
  { MLTAG_NETWORK, WEBKIT_DOWNLOAD_ERROR_NETWORK },
  { MLTAG_DESTINATION, WEBKIT_DOWNLOAD_ERROR_DESTINATION },
  { MLTAG_CANCELLED_BY_USER, WEBKIT_DOWNLOAD_ERROR_CANCELLED_BY_USER },
};

CAMLprim value ml_webkit_download_get_tables ()
{
  static const lookup_info *ml_lookup_tables[] = {
    ml_table_webkit_download_status,
    ml_table_webkit_download_error,
  };
  return (value)ml_lookup_tables;}
