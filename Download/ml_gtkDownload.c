#include "ml_gtkWebkit.h"
#include "ml_gtkDownload_tags_c.h"

#define WebkitDownloadStatus_val Webkit_download_status_val
#define WebkitDownloadError_val Webkit_download_error_val

ML_0(webkit_download_get_type, Val_gtype)

ML_1(webkit_download_new, WebkitNetworkRequest_val, Val_webkit_download)

ML_1(webkit_download_start, WebkitDownload_val, Unit)

ML_1(webkit_download_cancel, WebkitDownload_val, Unit)

ML_1(webkit_download_get_uri, WebkitDownload_val, Val_string)

ML_1(webkit_download_get_network_request, WebkitDownload_val, Val_webkit_network_request)

ML_1(webkit_download_get_network_response, WebkitDownload_val, Val_webkit_network_response)

ML_1(webkit_download_get_suggested_filename, WebkitDownload_val, Val_string)

ML_1(webkit_download_get_destination_uri, WebkitDownload_val, Val_string)

ML_2(webkit_download_set_destination_uri, WebkitDownload_val, String_val, Unit)

ML_1(webkit_download_get_progress, WebkitDownload_val, Val_double)

ML_1(webkit_download_get_elapsed_time, WebkitDownload_val, Val_double)

ML_1(webkit_download_get_total_size, WebkitDownload_val, Val_uint64_t)

ML_1(webkit_download_get_current_size, WebkitDownload_val, Val_uint64_t)

ML_1(webkit_download_get_status, WebkitDownload_val, Val_webkit_download_status)

