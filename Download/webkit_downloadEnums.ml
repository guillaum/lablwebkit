(** webkit_download enums *)

open Gpointer

type webkit_download_status =
  [ `FINISHED | `CANCELLED | `STARTED | `CREATED | `ERROR ]
type webkit_download_error = [ `NETWORK | `DESTINATION | `CANCELLED_BY_USER ]

(**/**)

external _get_tables : unit ->
    webkit_download_status variant_table
  * webkit_download_error variant_table
  = "ml_webkit_download_get_tables"


let webkit_download_status, webkit_download_error = _get_tables ()

let webkit_download_status_conv = Gobject.Data.enum webkit_download_status
let webkit_download_error_conv = Gobject.Data.enum webkit_download_error
