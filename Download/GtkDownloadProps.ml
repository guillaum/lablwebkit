open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module Download = struct
  let cast w : download obj = try_cast w "WebkitDownload"
  module P = struct
    let network_request : ([>`download],_) property =
      {name="network-request"; conv=GNetworkRequest.conv_networkrequest}
    let destination_uri : ([>`download],_) property =
      {name="destination-uri"; conv=string}
    let suggested_filename : ([>`download],_) property =
      {name="suggested-filename"; conv=string}
    let progress : ([>`download],_) property = {name="progress"; conv=double}
    let status : ([>`download],_) property =
      {name="status"; conv=webkit_download_status_conv}
    let current_size : ([>`download],_) property =
      {name="current-size"; conv=uint64}
    let total_size : ([>`download],_) property =
      {name="total-size"; conv=uint64}
    let network_response : ([>`download],_) property =
      {name="network-response"; conv=GNetworkResponse.conv_networkresponse}
  end
  module S = struct
    open GtkSignal
    let error =
      {name="error"; classe=`download; marshaller=fun f ->
       marshal3_ret ~ret:boolean int int string "WebkitDownload::error" f}
  end
  let create ?network_request ?network_response pl : download obj =
    let pl = (
      may_cons P.network_request network_request (
      may_cons P.network_response network_response pl)) in
    Gobject.unsafe_create "WebkitDownload" pl
  let make_params ~cont pl ?destination_uri =
    let pl = (may_cons P.destination_uri destination_uri pl) in
    cont pl
end

