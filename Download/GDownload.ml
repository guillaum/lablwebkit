open GObj
open Gobject
open Gtk
open GtkDownload
open OGtkDownloadProps

class download_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit download_props
  method start = Download.start obj_
  method cancel = Download.cancel obj_
  method get_uri = Download.get_uri obj_
  method get_elapsed_time = Download.get_elapsed_time obj_
end

class download_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit download_sigs
end

class download obj_ = object(self)
  method as_download : GtkWebkitTypes.download obj = obj_
  inherit download_skel obj_
  method connect = new download_signals obj_
end

let download request =
  new download (Download.create request)

let wrap_download obj = new download (unsafe_cast obj)
let unwrap_download obj = unsafe_cast obj#as_download
let conv_download =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_download c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_download c))) }

