/* webkit_download_status : tags and macros */
#define MLTAG_FINISHED	((value)(-292281486*2+1))
#define MLTAG_CANCELLED	((value)(495777393*2+1))
#define MLTAG_STARTED	((value)(888077601*2+1))
#define MLTAG_CREATED	((value)(289311176*2+1))
#define MLTAG_ERROR	((value)(-250084440*2+1))

extern const lookup_info ml_table_webkit_download_status[];
#define Val_webkit_download_status(data) ml_lookup_from_c (ml_table_webkit_download_status, data)
#define Webkit_download_status_val(key) ml_lookup_to_c (ml_table_webkit_download_status, key)

/* webkit_download_error : tags and macros */
#define MLTAG_NETWORK	((value)(-644835186*2+1))
#define MLTAG_DESTINATION	((value)(-399010194*2+1))
#define MLTAG_CANCELLED_BY_USER	((value)(344316389*2+1))

extern const lookup_info ml_table_webkit_download_error[];
#define Val_webkit_download_error(data) ml_lookup_from_c (ml_table_webkit_download_error, data)
#define Webkit_download_error_val(key) ml_lookup_to_c (ml_table_webkit_download_error, key)

