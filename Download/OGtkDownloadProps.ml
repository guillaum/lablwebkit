open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkDownloadProps

class virtual download_props = object
  val virtual obj : _ obj
  method set_destination_uri = set Download.P.destination_uri obj
  method network_request = get Download.P.network_request obj
  method destination_uri = get Download.P.destination_uri obj
  method suggested_filename = get Download.P.suggested_filename obj
  method progress = get Download.P.progress obj
  method status = get Download.P.status obj
  method current_size = get Download.P.current_size obj
  method total_size = get Download.P.total_size obj
  method network_response = get Download.P.network_response obj
end

class virtual download_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method network_request = self#notify Download.P.network_request
  method destination_uri = self#notify Download.P.destination_uri
  method suggested_filename = self#notify Download.P.suggested_filename
  method progress = self#notify Download.P.progress
  method status = self#notify Download.P.status
  method current_size = self#notify Download.P.current_size
  method total_size = self#notify Download.P.total_size
  method network_response = self#notify Download.P.network_response
end

class virtual download_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method error = self#connect Download.S.error
  method notify_network_request ~callback =
    self#notify Download.P.network_request ~callback
  method notify_destination_uri ~callback =
    self#notify Download.P.destination_uri ~callback
  method notify_suggested_filename ~callback =
    self#notify Download.P.suggested_filename ~callback
  method notify_progress ~callback =
    self#notify Download.P.progress ~callback
  method notify_status ~callback = self#notify Download.P.status ~callback
  method notify_current_size ~callback =
    self#notify Download.P.current_size ~callback
  method notify_total_size ~callback =
    self#notify Download.P.total_size ~callback
  method notify_network_response ~callback =
    self#notify Download.P.network_response ~callback
end

