open Gobject
open Data
open GtkWebkitTypes
module Download = struct
  include GtkDownloadProps.Download
  external create : networkrequest Gtk.obj -> download Gtk.obj = "ml_webkit_download_new"
  external start : download Gtk.obj -> unit = "ml_webkit_download_start"
  external cancel : download Gtk.obj -> unit = "ml_webkit_download_cancel"
  external get_uri : download Gtk.obj -> string = "ml_webkit_download_get_uri"
  external get_elapsed_time : download Gtk.obj -> float = "ml_webkit_download_get_elapsed_time"
end