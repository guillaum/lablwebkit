open Gobject
open Data
open GtkWebkitTypes
module WebResource = struct
  include GtkWebResourceProps.WebResource
  external create : string -> Int32.t -> string -> string -> string -> string -> webresource Gtk.obj = "webkit_web_resource_new" "ml_webkit_web_resource_new_bc"
  external get_data : webresource Gtk.obj -> string = "ml_webkit_web_resource_get_data"
end