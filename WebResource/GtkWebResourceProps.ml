open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebResource = struct
  let cast w : webresource obj = try_cast w "WebkitWebResource"
  module P = struct
    let uri : ([>`webresource],_) property = {name="uri"; conv=string}
    let mime_type : ([>`webresource],_) property =
      {name="mime-type"; conv=string}
    let encoding : ([>`webresource],_) property =
      {name="encoding"; conv=string}
    let frame_name : ([>`webresource],_) property =
      {name="frame-name"; conv=string}
  end
  module S = struct
    open GtkSignal
    let response_received =
      {name="response_received"; classe=`webresource; marshaller=fun f ->
       marshal1 GNetworkResponse.conv_networkresponse
         "WebkitWebResource::response_received" f}
    let load_finished =
      {name="load_finished"; classe=`webresource; marshaller=marshal_unit}
    let content_length_received =
      {name="content_length_received"; classe=`webresource;
       marshaller=fun f ->
       marshal1 int "WebkitWebResource::content_length_received" f}
  end
  let create ?uri pl : webresource obj =
    let pl = (may_cons P.uri uri pl) in
    Gobject.unsafe_create "WebkitWebResource" pl
end

