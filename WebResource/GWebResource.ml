open GObj
open Gobject
open Gtk
open GtkWebResource
open OGtkWebResourceProps

class web_resource_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_resource_props
  method get_data = WebResource.get_data obj_
end

class web_resource_signals obj_ = object(self)
  inherit ['a] gobject_signals obj_
  inherit web_resource_sigs
end

class web_resource obj_ = object(self)
  method as_webresource : GtkWebkitTypes.webresource obj = obj_
  inherit web_resource_skel obj_
  method connect = new web_resource_signals obj_
end

let web_resource data size uri mime_type encoding frame_name =
  new web_resource (WebResource.create data size uri mime_type encoding frame_name)

let wrap_webresource obj = new web_resource (unsafe_cast obj)
let unwrap_webresource obj = unsafe_cast obj#as_webresource
let conv_webresource =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webresource c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webresource c))) }

