open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebResourceProps

class virtual web_resource_props = object
  val virtual obj : _ obj
  method uri = get WebResource.P.uri obj
  method mime_type = get WebResource.P.mime_type obj
  method encoding = get WebResource.P.encoding obj
  method frame_name = get WebResource.P.frame_name obj
end

class virtual web_resource_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method uri = self#notify WebResource.P.uri
  method mime_type = self#notify WebResource.P.mime_type
  method encoding = self#notify WebResource.P.encoding
  method frame_name = self#notify WebResource.P.frame_name
end

class virtual web_resource_sigs = object (self)
  method private virtual connect :
    'b. ('a,'b) GtkSignal.t -> callback:'b -> GtkSignal.id
  method private virtual notify :
    'b. ('a,'b) property -> callback:('b -> unit) -> GtkSignal.id
  method response_received = self#connect WebResource.S.response_received
  method load_finished = self#connect WebResource.S.load_finished
  method content_length_received =
    self#connect WebResource.S.content_length_received
  method notify_uri ~callback = self#notify WebResource.P.uri ~callback
  method notify_mime_type ~callback =
    self#notify WebResource.P.mime_type ~callback
  method notify_encoding ~callback =
    self#notify WebResource.P.encoding ~callback
  method notify_frame_name ~callback =
    self#notify WebResource.P.frame_name ~callback
end

