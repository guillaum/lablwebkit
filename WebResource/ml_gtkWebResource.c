#include "ml_gtkWebkit.h"
#include "ml_gtkWebResource_tags_c.h"



ML_0(webkit_web_resource_get_type, Val_gtype)

ML_6(webkit_web_resource_new, String_val, Gssize_val, String_val, String_val, String_val, String_val, Val_webkit_web_resource)
CAMLprim value ml_webkit_web_resource_new_bc(value *argv, int argc) {
  return ml_webkit_web_resource_new(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}

ML_1(webkit_web_resource_get_data, WebkitWebResource_val, Val_gstring)

ML_1(webkit_web_resource_get_uri, WebkitWebResource_val, Val_string)

ML_1(webkit_web_resource_get_mime_type, WebkitWebResource_val, Val_string)

ML_1(webkit_web_resource_get_encoding, WebkitWebResource_val, Val_string)

ML_1(webkit_web_resource_get_frame_name, WebkitWebResource_val, Val_string)

