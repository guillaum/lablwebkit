open Gobject
open Data
open GtkWebkitTypes
module WebHistoryItem = struct
  include GtkWebHistoryItemProps.WebHistoryItem
  external create : unit -> webhistoryitem Gtk.obj = "ml_webkit_web_history_item_new"
  external new_with_data : string -> string -> webhistoryitem Gtk.obj = "ml_webkit_web_history_item_new_with_data"
  external copy : webhistoryitem Gtk.obj -> webhistoryitem Gtk.obj = "ml_webkit_web_history_item_copy"
end