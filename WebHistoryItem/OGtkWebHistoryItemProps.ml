open GtkSignal
open Gobject
open Data
let set = set
let get = get
let param = param
open GtkWebHistoryItemProps

class virtual web_history_item_props = object
  val virtual obj : _ obj
  method set_alternate_title = set WebHistoryItem.P.alternate_title obj
  method title = get WebHistoryItem.P.title obj
  method alternate_title = get WebHistoryItem.P.alternate_title obj
  method uri = get WebHistoryItem.P.uri obj
  method original_uri = get WebHistoryItem.P.original_uri obj
  method last_visited_time = get WebHistoryItem.P.last_visited_time obj
end

class virtual web_history_item_notify obj = object (self)
  val obj : 'a obj = obj
  method private notify : 'b. ('a, 'b) property ->
    callback:('b -> unit) -> _ =
  fun prop ~callback -> GtkSignal.connect_property obj
    ~prop ~callback
  method title = self#notify WebHistoryItem.P.title
  method alternate_title = self#notify WebHistoryItem.P.alternate_title
  method uri = self#notify WebHistoryItem.P.uri
  method original_uri = self#notify WebHistoryItem.P.original_uri
  method last_visited_time = self#notify WebHistoryItem.P.last_visited_time
end

