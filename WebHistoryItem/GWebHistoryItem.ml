open GObj
open Gobject
open Gtk
open GtkWebHistoryItem
open OGtkWebHistoryItemProps

class web_history_item_skel obj_ = object(self)
  val obj = obj_
  method private obj = obj_
  inherit web_history_item_props
  method copy = WebHistoryItem.copy obj_
end

class web_history_item obj_ = object(self)
  method as_webhistoryitem : GtkWebkitTypes.webhistoryitem obj = obj_
  inherit web_history_item_skel obj_
end

let web_history_item () =
  new web_history_item (WebHistoryItem.create ())

let web_history_item_with_data uri title =
  new web_history_item (WebHistoryItem.new_with_data uri title)

let wrap_webhistoryitem obj = new web_history_item (unsafe_cast obj)
let unwrap_webhistoryitem obj = unsafe_cast obj#as_webhistoryitem
let conv_webhistoryitem =
  { kind = `OBJECT;
    proj = (function `OBJECT (Some c) -> wrap_webhistoryitem c
         | `OBJECT None -> raise Gpointer.Null
         | _ -> failwith "GObj.get_object");
    inj = (fun c -> `OBJECT (Some (unwrap_webhistoryitem c))) }

