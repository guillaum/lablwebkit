#include "ml_gtkWebkit.h"
#include "ml_gtkWebHistoryItem_tags_c.h"



ML_0(webkit_web_history_item_get_type, Val_gtype)

ML_0(webkit_web_history_item_new, Val_webkit_web_history_item)

ML_2(webkit_web_history_item_new_with_data, String_val, String_val, Val_webkit_web_history_item)

ML_1(webkit_web_history_item_get_title, WebkitWebHistoryItem_val, Val_string)

ML_1(webkit_web_history_item_get_alternate_title, WebkitWebHistoryItem_val, Val_string)

ML_2(webkit_web_history_item_set_alternate_title, WebkitWebHistoryItem_val, String_val, Unit)

ML_1(webkit_web_history_item_get_uri, WebkitWebHistoryItem_val, Val_string)

ML_1(webkit_web_history_item_get_original_uri, WebkitWebHistoryItem_val, Val_string)

ML_1(webkit_web_history_item_get_last_visited_time, WebkitWebHistoryItem_val, Val_double)

ML_1(webkit_web_history_item_copy, WebkitWebHistoryItem_val, Val_webkit_web_history_item)

