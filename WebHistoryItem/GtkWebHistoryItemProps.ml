open Gobject
open Data

open GtkWebkitTypes

let may_cons = Property.may_cons
let may_cons_opt = Property.may_cons_opt

module WebHistoryItem = struct
  let cast w : webhistoryitem obj = try_cast w "WebkitWebHistoryItem"
  module P = struct
    let title : ([>`webhistoryitem],_) property = {name="title"; conv=string}
    let alternate_title : ([>`webhistoryitem],_) property =
      {name="alternate-title"; conv=string}
    let uri : ([>`webhistoryitem],_) property = {name="uri"; conv=string}
    let original_uri : ([>`webhistoryitem],_) property =
      {name="original-uri"; conv=string}
    let last_visited_time : ([>`webhistoryitem],_) property =
      {name="last-visited-time"; conv=double}
  end
  let create pl : webhistoryitem obj =
    Gobject.unsafe_create "WebkitWebHistoryItem" pl
  let make_params ~cont pl ?alternate_title =
    let pl = (may_cons P.alternate_title alternate_title pl) in
    cont pl
end

